using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace CommandCenter.Controllers
{
    [Route("api/ConnectedDeviceSummary")]
    public class ConnectedDeviceSummaryController : Controller
    {
        private readonly ILogger _logger;

        private IConnectedDeviceSummary repository;
        private readonly IHostingEnvironment _hostingEnvironment;
        List<ConnectedDeviceSummary> records = new List<ConnectedDeviceSummary>();
        public ConnectedDeviceSummaryController(IHostingEnvironment hostingEnvironment, IConfiguration confg, ILogger<ConnectedDeviceSummaryController> logger)
        {
            _hostingEnvironment = hostingEnvironment;        
            repository = new ConnectedDeviceSummaryRepository(confg);
            _logger = logger;
        }
        // GET api/values
        [HttpGet]
        [Route("{GeoLocationId}")]
        public List<ConnectedDeviceSummary> Get(int GeoLocationId)
        {
            
            _logger.LogInformation(LoggingEvents.GetItem, "Getting ConnectedDeviceSummary API - api/ConnectedDeviceSummary/geolocationId based on GeoLocationId : {GeoLocationId}", GeoLocationId);
            try
            {
                records = repository.GetAll(GeoLocationId);
                _logger.LogInformation(LoggingEvents.ListItems, "Got ConnectedDeviceSummary API - api/ConnectedDeviceSummary/geolocat based on GeoLocationId : {GeoLocationId}", GeoLocationId);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message, GeoLocationId);
            }
          

            return records;
        }

        [HttpGet]
        [Route("{GeoLocationId}/{SBG}")]
        public List<ConnectedDeviceSummary> GetbySBG(int GeoLocationId, string sbg)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting ConnectedDeviceSummary API - api/ConnectedDeviceSummary/Geolocation/sbg based on GeoLocationId & SBG : {GeoLocationId}", GeoLocationId);
            try
            {
                records = repository.GetbySBG(GeoLocationId, sbg);
                _logger.LogInformation(LoggingEvents.ListItems, "Got ConnectedDeviceSummary API - api/ConnectedDeviceSummary/geolocation/sbg based on GeoLocationId & SBG : {GeoLocationId}", GeoLocationId);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message, GeoLocationId);
            }
           
            return records;
        }
    }
}
