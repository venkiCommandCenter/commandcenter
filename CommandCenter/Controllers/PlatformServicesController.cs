using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;
using System;
using Microsoft.Extensions.Logging;

namespace CommandCenter.Controllers
{

    [Route("api/PlatformServices")]
    public class PlatformServicesController : Controller
    {
        IPlatformServices repository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger _logger;

        PlatformServices platformServices;

        public PlatformServicesController(IHostingEnvironment hostingEnvironment, IConfiguration confg, ILogger<PlatformServicesController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new PlatformServicesRepository(confg);
            _logger = logger;
        }
        // GET api/values
        [HttpGet]
        public PlatformServices Get()
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting Platform services API - api/PlatformServices");
            try
            {
                platformServices = repository.GetPlatformServices();
                _logger.LogInformation(LoggingEvents.ListItems, "Got Platform Services API - api/PlatformServices");
            }
            catch (Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message);
            }

            return platformServices;
        }
    }
}
