using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using OfficeOpenXml;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Logging;

namespace CommandCenter.Controllers
{
    [Route("api/ConnectedDevicePlot")]
    public class ConnectedDevicePlotController : Controller
    {
        private IConnectedDevicePlot repository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ILogger _logger;

        IEnumerable<ConnectedDevicePlot> records;

        public ConnectedDevicePlotController(IHostingEnvironment hostingEnvironment, IConfiguration confg, ILogger<ConnectedDevicePlotController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new ConnectedDevicePlotRepository(confg);
            _logger = logger;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<ConnectedDevicePlot> Get()
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting ConnectedDevicePlot Globally API - api/ConnectedDevicePlot");
            try
            {
                records = repository.GetAll();
                _logger.LogInformation(LoggingEvents.ListItems, "Got ConnectedDevicePlot Globally API - api/ConnectedDevicePlot");
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message);
            }
            return records;
        }

        [HttpGet]
        [Route("{SBG}")]
        public IEnumerable<ConnectedDevicePlot> GetbySBG(string sbg)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting ConnectedDevicePlot API - api/ConnectedDevicePlot/sbg based on sbg : {sbg}", sbg);
            try
            {
                records = repository.GetbySBG(sbg);
                _logger.LogInformation(LoggingEvents.ListItems, "Got ConnectedDevicePlot API - api/ConnectedDevicePlot/sbg based on SBG : {sbg}", sbg);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message, sbg);
            }
           
            return records;
        }

        [HttpPost]
        [Route("Upload")]
        public bool Upload([FromForm] IFormFile files)
        {
            //validate if the file object is preset
            if (files == null) throw new Exception("File is null");
            //Validate if the file has some content
            if (files.Length == 0) throw new Exception("File is empty");


            //.net core get the host root path i.e., wwwroot
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFilePath = files.FileName; //file name from param

            FileInfo fileInfo = new FileInfo(Path.Combine(sWebRootFolder, sFilePath));
            //check if file exist
            if (fileInfo.Exists)
            {
                fileInfo.Delete(); //delete if any
            }

            //create the new file uploaded            
            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFilePath), FileMode.Create))
            {
                files.CopyToAsync(stream);
                Thread.Sleep(1000);
            }

            try
            {
                using (ExcelPackage package = new ExcelPackage(fileInfo))
                {
                    repository.Delete();

                    //Upload connected device
                    UploadConnectedDevicePlot(package);
                    //Upload Conneceted device summary
                    UploadConnectedDeviceSummary(package);
                    //Upload Outages device
                    UploadOutagesDevicePlot(package);
                    //Upload Outages device summary
                    UploadOutagesDeviceSummary(package);
                    //UploadPlatformUtilization
                    UploadPlatformUtilization(package);
                    //UploadCloudServices
                    UploadCloudService(package);
                    //UploadTicketSummary
                    UploadTicketSummary(package);
                    //UploadNetworkAvailabality
                    UploadNetworkAvailabality(package);
                    //UploadAlertVolume
                    UploadAlertVolume(package);
                    //UploadNewsFeed
                    UploadNewsFeed(package);
                    //Upload Sentiance Healthcare
                    UploadSentianceHealth(package);
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// Upload Conneceted device plot for globe
        /// </summary>
        /// <param name="package"></param>
        private void UploadConnectedDevicePlot(ExcelPackage package)
        {
            //Intialize the object
            List<ConnectedDevicePlot> listConnectedDevice = new List<ConnectedDevicePlot>();
            // Conneceted Device Plot 
            ExcelWorksheet worksheet = package.Workbook.Worksheets["ConnectedDevicePlot"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            int rowCount = worksheet.Dimension.Rows;
            int colCount = worksheet.Dimension.Columns;
            for (int row = 1; row <= rowCount; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(worksheet.Cells[row, 1].Value.ToString()))
                    {
                        ConnectedDevicePlot connectedDevice = new ConnectedDevicePlot()
                        {
                            Lattitude = worksheet.Cells[row, 1].Value.ToString(),
                            Longitude = worksheet.Cells[row, 2].Value.ToString(),
                            Intensity = worksheet.Cells[row, 3].Value.ToString(),
                            DeviceCount = worksheet.Cells[row, 4].Value.ToString()
                        };
                        listConnectedDevice.Add(connectedDevice);
                    }
                }
            }
            repository.Upload(listConnectedDevice, "ConnectedDevicePlot");
        }

        /// <summary>
        /// Upload Connected Deivce Summary
        /// </summary>
        /// <param name="package"></param>
        private void UploadConnectedDeviceSummary(ExcelPackage package)
        {
            //ConnecetedDeviceSummary
            List<ConnectedDeviceSummary> listConnecetedDeviceSummary = new List<ConnectedDeviceSummary>();
            ExcelWorksheet connecetedDeviceSummaryworksheet = package.Workbook.Worksheets["ConnectedDeviceSummary"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            //int rowCount = worksheet.Dimension.Rows;
            //int colCount = worksheet.Dimension.Columns;
            for (int row = 1; row <= connecetedDeviceSummaryworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(connecetedDeviceSummaryworksheet.Cells[row, 1].Value as string))
                    {
                        ConnectedDeviceSummary connectedDeviceSummary = new ConnectedDeviceSummary()
                        {

                            ConnectedDeviceLabel = connecetedDeviceSummaryworksheet.Cells[row, 1].Value.ToString(),
                            Value = connecetedDeviceSummaryworksheet.Cells[row, 2].Value.ToString(),
                            Units = connecetedDeviceSummaryworksheet.Cells[row, 3].Value.ToString()
                        };
                        listConnecetedDeviceSummary.Add(connectedDeviceSummary);
                    }
                }
            }
            repository.Upload(listConnecetedDeviceSummary, "ConnectedDeviceSummary");
        }

        /// <summary>
        /// Upload Outages Device Plot for the globe
        /// </summary>
        /// <param name="package"></param>
        private void UploadOutagesDevicePlot(ExcelPackage package)
        {
            //Intialize the object
            List<NorthAmericaDevicePlot> listOutageDevice = new List<NorthAmericaDevicePlot>();
            // Conneceted Device Plot 
            ExcelWorksheet worksheetOutagePlot = package.Workbook.Worksheets["NorthAmericaDevicePlot"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= worksheetOutagePlot.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(worksheetOutagePlot.Cells[row, 1].Value.ToString()))
                    {
                        NorthAmericaDevicePlot outagesDevice = new NorthAmericaDevicePlot()
                        {
                            Lattitude = worksheetOutagePlot.Cells[row, 1].Value.ToString(),
                            Longitude = worksheetOutagePlot.Cells[row, 2].Value.ToString(),
                            DeviceCount = worksheetOutagePlot.Cells[row, 3].Value.ToString(),
                            Intensity = worksheetOutagePlot.Cells[row, 4].Value.ToString(),
                            GeoLocation = worksheetOutagePlot.Cells[row, 5].Value.ToString()
                        };
                        listOutageDevice.Add(outagesDevice);
                    }
                }
            }
            repository.Upload(listOutageDevice, "NorthAmericaDevicePlot");
        }

        /// <summary>
        /// Uplaod Outages device summary data
        /// </summary>
        /// <param name="package"></param>
        private void UploadOutagesDeviceSummary(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<NorthAmericaDeviceSummary> listOutagesDeviceSummary = new List<NorthAmericaDeviceSummary>();
            ExcelWorksheet outagesDeviceSummaryworksheet = package.Workbook.Worksheets["NorthAmericaDeviceSummary"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= outagesDeviceSummaryworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(outagesDeviceSummaryworksheet.Cells[row, 1].Value as string))
                    {
                        NorthAmericaDeviceSummary outagesDeviceSummary = new NorthAmericaDeviceSummary()
                        {
                            Label = outagesDeviceSummaryworksheet.Cells[row, 1].Value.ToString(),
                            Value = outagesDeviceSummaryworksheet.Cells[row, 2].Value.ToString(),
                            Unit = outagesDeviceSummaryworksheet.Cells[row, 3].Value.ToString(),
                            GeoLocation = outagesDeviceSummaryworksheet.Cells[row, 4].Value.ToString()
                        };
                        listOutagesDeviceSummary.Add(outagesDeviceSummary);
                    }
                }
            }
            repository.Upload(listOutagesDeviceSummary, "NorthAmericaDeviceSummary");
        }

        /// <summary>
        /// Upload Platform Utilization data
        /// </summary>
        /// <param name="package"></param>
        private void UploadPlatformUtilization(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<PlatformUtilization> listPlatformUtilization = new List<PlatformUtilization>();
            ExcelWorksheet platformUtilizationworksheet = package.Workbook.Worksheets["PlatformUtilization"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= platformUtilizationworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(platformUtilizationworksheet.Cells[row, 1].Value as string))
                    {
                        PlatformUtilization platformUtilization = new PlatformUtilization()
                        {
                            PlatformName = platformUtilizationworksheet.Cells[row, 1].Value.ToString(),
                            MaxUtilization = platformUtilizationworksheet.Cells[row, 2].Value.ToString(),
                            MinUtilization = platformUtilizationworksheet.Cells[row, 3].Value.ToString(),
                            CurrentUtilization = platformUtilizationworksheet.Cells[row, 4].Value.ToString(),
                            CurrentState = platformUtilizationworksheet.Cells[row, 5].Value.ToString(),
                            PlatformUnit = platformUtilizationworksheet.Cells[row, 6].Value as string

                        };
                        listPlatformUtilization.Add(platformUtilization);
                    }
                }
            }
            repository.Upload(listPlatformUtilization, tableName: "PlatformUtilization");
        }

        /// <summary>
        /// Upload Cloud Services
        /// </summary>
        /// <param name="package"></param>
        private void UploadCloudService(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<CloudService> listCloudService = new List<CloudService>();
            ExcelWorksheet cloudServiceworksheet = package.Workbook.Worksheets["CloudServices"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= cloudServiceworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(cloudServiceworksheet.Cells[row, 1].Value as string))
                    {
                        CloudService cloudService = new CloudService()
                        {
                            CloudServiceName = cloudServiceworksheet.Cells[row, 1].Value.ToString(),
                            CloudServicePercentage = cloudServiceworksheet.Cells[row, 2].Value.ToString(),
                            CurrentStatus = cloudServiceworksheet.Cells[row, 3].Value.ToString()
                        };
                        listCloudService.Add(cloudService);
                    }
                }
            }
            repository.Upload(listCloudService, tableName: "CloudServices");
        }

        /// <summary>
        /// Upload Ticket Summary
        /// </summary>
        /// <param name="package"></param>
        private void UploadTicketSummary(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<TicketSummary> listTicketSummary = new List<TicketSummary>();
            ExcelWorksheet ticketSummaryworksheet = package.Workbook.Worksheets["TicketSummary"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= ticketSummaryworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(ticketSummaryworksheet.Cells[row, 1].Value as string))
                    {
                        TicketSummary ticketSummary = new TicketSummary()
                        {
                            SummaryDetails = ticketSummaryworksheet.Cells[row, 1].Value.ToString(),
                            SummaryValue = ticketSummaryworksheet.Cells[row, 2].Value.ToString()
                        };
                        listTicketSummary.Add(ticketSummary);
                    }
                }
            }
            repository.Upload(listTicketSummary, tableName: "TicketSummary");
        }

        /// <summary>
        /// Upload Network Availabality
        /// </summary>
        /// <param name="package"></param>
        private void UploadNetworkAvailabality(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<NetworkAvailabality> listNetworkAvailabality = new List<NetworkAvailabality>();
            ExcelWorksheet networkAvailabalityworksheet = package.Workbook.Worksheets["NetworkAvailabality"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= networkAvailabalityworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(networkAvailabalityworksheet.Cells[row, 1].Value as string))
                    {
                        NetworkAvailabality networkAvailabality = new NetworkAvailabality()
                        {
                            Region = networkAvailabalityworksheet.Cells[row, 1].Value.ToString(),
                            CurrentStatus = networkAvailabalityworksheet.Cells[row, 2].Value.ToString()
                        };
                        listNetworkAvailabality.Add(networkAvailabality);
                    }
                }
            }
            repository.Upload(listNetworkAvailabality, tableName: "NetworkAvailabality");
        }

        /// <summary>
        /// Upload Alert Volume
        /// </summary>
        /// <param name="package"></param>
        private void UploadAlertVolume(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<AlertVoulme> listAlertVolume = new List<AlertVoulme>();
            ExcelWorksheet alertVolumeworksheet = package.Workbook.Worksheets["AlertVoulme"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= alertVolumeworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(alertVolumeworksheet.Cells[row, 1].Value.ToString()))
                    {
                        AlertVoulme alertVolume = new AlertVoulme()
                        {
                            AlertVolumeTime = Convert.ToDateTime(alertVolumeworksheet.Cells[row, 1].Value.ToString()),
                            AlertVolume = Convert.ToInt32(alertVolumeworksheet.Cells[row, 2].Value.ToString())
                        };
                        listAlertVolume.Add(alertVolume);
                    }
                }
            }
            repository.Upload(listAlertVolume, tableName: "AlertVolume");

        }

        /// <summary>
        /// Upload News Feed
        /// </summary>
        /// <param name="package"></param>
        private void UploadNewsFeed(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<NewsFeed> listNewsFeed = new List<NewsFeed>();
            ExcelWorksheet newsFeedworksheet = package.Workbook.Worksheets["NewsFeedAlert"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= newsFeedworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(newsFeedworksheet.Cells[row, 1].Value as string))
                    {
                        NewsFeed newsFeed = new NewsFeed()
                        {
                            CurrentNewsFeed = newsFeedworksheet.Cells[row, 1].Value.ToString(),
                            NextNewsFeed = newsFeedworksheet.Cells[row, 2].Value.ToString(),
                            TomorrowNewsFeed = newsFeedworksheet.Cells[row, 3].Value.ToString()
                        };
                        listNewsFeed.Add(newsFeed);
                    }
                }
            }
            repository.Upload(listNewsFeed, tableName: "NewsFeedAlert");

        }

        private void UploadSentianceHealth(ExcelPackage package)
        {
            //OutageDeviceSummary
            List<SentianceHealth> listSentianceHealth = new List<SentianceHealth>();
            ExcelWorksheet sentianceHealthworksheet = package.Workbook.Worksheets["SentianceHealth"]; //worksheet for ConnectedDeviceGlobePlot data
            Thread.Sleep(1000);
            for (int row = 1; row <= sentianceHealthworksheet.Dimension.Rows; row++)
            {
                if (row > 1)
                {
                    if (!String.IsNullOrEmpty(sentianceHealthworksheet.Cells[row, 1].Value as string))
                    {
                        SentianceHealth sentianceHealth = new SentianceHealth()
                        {
                            ProductName = sentianceHealthworksheet.Cells[row, 1].Value.ToString(),
                            SLA = sentianceHealthworksheet.Cells[row, 2].Value.ToString(),
                            CurrentStatus = sentianceHealthworksheet.Cells[row, 3].Value.ToString()
                        };
                        listSentianceHealth.Add(sentianceHealth);
                    }
                }
            }
            repository.Upload(listSentianceHealth, tableName: "NewsFeedAlert");
        }

    }
}
