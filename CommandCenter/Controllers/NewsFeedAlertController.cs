using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;

namespace CommandCenter.Controllers
{
    
    [Route("api/NewsFeedAlert")]
    public class NewsFeedAlertController : Controller
    {
        INewsFeed repository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public NewsFeedAlertController(IHostingEnvironment hostingEnvironment, IConfiguration confg)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new NewsFeedRepository(confg);

        }
        // GET api/values
        [HttpGet]
        public NewsFeed Get()
        {
            return repository.GetAll();
        }
    }
}
