﻿using CommandCenter.Interfaces;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Text;

namespace CommandCenter.Controllers
{
    [Route("api/DownloadKPIData")]
    public class DownloadKPIDataController : Controller
    {
        private IDownloadKPIData repository;
        //OutagesDeviceSummaryRepository repository = new OutagesDeviceSummaryRepository();
        private readonly IHostingEnvironment _hostingEnvironment;

        private readonly ILogger _logger;

        byte[] temp;

        public DownloadKPIDataController(IHostingEnvironment hostingEnvironment, IConfiguration confg, ILogger<DownloadKPIDataController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new DownloadKPIDataRepository(confg);
            _logger = logger;
        }

        // GET api/values
        [HttpGet]
        [Route("{GeoLocationId}")]
        public ActionResult Get(int GeoLocationId, string sbg = null)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Downloading the KPI data Process begins API - api/DownloadKPIData/Geolocation based on Geolocation", GeoLocationId);
            try
            {
                DownloadKPIData lstKPIData = new DownloadKPIData();
                lstKPIData = repository.GetReport(GeoLocationId, sbg);
                StringBuilder str = GenerateExcel(lstKPIData);

                HttpContext.Response.Headers.Add("content-disposition", "attachment; filename=SentienceDashboard" + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());

                _logger.LogInformation(LoggingEvents.GetItem, "Downloaded the KPI data API - api/DownloadKPIData/Geolocation based on geolocation", GeoLocationId);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message, GeoLocationId);
            }
            return File(temp, "application/vnd.ms-excel");
        }

        [HttpGet]
        [Route("{GeoLocationId}/{SBG}")]
        public ActionResult GetConnected(int GeoLocationId, string sbg)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Downloading the KPI data Process begins API - api/DownloadKPIData/Geolocation/sbg based on geolocation and SBG", GeoLocationId);
            try
            {
                DownloadKPIData lstKPIData = new DownloadKPIData();
                lstKPIData = repository.GetReport(GeoLocationId, sbg);
                StringBuilder str = GenerateExcel(lstKPIData);

                HttpContext.Response.Headers.Add("content-disposition", "attachment; filename=SentienceDashboard" + ".xls");
                this.Response.ContentType = "application/vnd.ms-excel";
                temp = System.Text.Encoding.UTF8.GetBytes(str.ToString());

                _logger.LogInformation(LoggingEvents.GetItem, "Downloaded the KPI data API - api/DownloadKPIData/Geolocation/sbg based on geolocation and SBG", GeoLocationId + "" + sbg);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message, GeoLocationId);
            }
            return File(temp, "application/vnd.ms-excel");
        }

        private static StringBuilder GenerateExcel(DownloadKPIData lstKPIData)
        {
            //Application ExcelApp = new Application();
            //Workbook ExcelWorkBook = null;
            //Worksheet ExcelWorkSheet = null;

            //ExcelApp.Visible = true;
            //ExcelWorkBook = ExcelApp.Workbooks.Add(XlWBATemplate.xlWBATWorksheet);
            //List<string> SheetNames = new List<string>();
            //SheetNames.Add("Connected Device Summary");
            //SheetNames.Add("Connected Plot Summary");

            //try
            //{
            //    for (int i = 0; i < lstKPIData.lstConnectedDevicePlot.Count; i++)
            //    {
            //        int len = lstKPIData.lstConnectedDevicePlot.Count;
            //    }
            //}
            //catch (System.Exception exHandle)
            //{
            //    Console.WriteLine("Exception: " + exHandle.Message);
            //    Console.ReadLine();
            //}


            StringBuilder str = new StringBuilder();

            #region String for Excel File
            str.Append("<table border=`" + "1px" + "`b>");
            //Connected Device Summary Region
            str.Append("<tr><b><font face=Arial Narrow size=3> Connected Device Summary</b> </tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>ConnectedDeviceLabel</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>Value</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>Units</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>SBG</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>Previous90Days</font></b></td>");
            str.Append("</tr>");
            foreach (ConnectedDeviceSummary val in lstKPIData.lstConnectedDeviceSummary)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.ConnectedDeviceLabel + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Value + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Units + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.SBG + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Previous90Days + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            str.Append("<table border=`" + "1px" + "`b>");
            //Platform Utilization Region
            str.Append("<tr><b><font face=Arial Narrow size=3> Platform Utilization Summary </b></tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>PlatformName</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>MaxUtilization</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>MinUtilization</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>CurrentUtilization</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>CurrentState</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>PlatformUnit</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>PlanStatus</font></b></td>");
            str.Append("</tr>");
            foreach (PlatformUtilization val in lstKPIData.lstPlatformUtilization)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.PlatformName + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.MaxUtilization + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.MinUtilization + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CurrentUtilization + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CurrentState + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.PlatformUnit + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.PlanStatus + "</font></td>");
                str.Append("</tr>");
            }

            str.Append("</table>");

            str.Append("<br/>");
            str.Append("<table border=`" + "1px" + "`b>");
            //Cloud Service Summary
            str.Append("<tr><b><font face=Arial Narrow size=3> Platform Service Summary </b></tr>");
            str.Append("<tr><b><u><font face=Arial Narrow size=" + "12px" + "> Cloud Service </u> </b></tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>CloudServiceName</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>CloudServicePercentage</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>CurrentStatus</font></b></td>");
            str.Append("</tr>");
            foreach (CloudService val in lstKPIData.platformServices.cloudServices)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CloudServiceName + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CloudServicePercentage + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CurrentStatus + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            str.Append("<table border=`" + "1px" + "`b>");
            //Network Availability Summary
            str.Append("<tr><b><u><font face=Arial Narrow size=" + "12px" + ">Network Availability</u></b></tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>Region</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>CurrentStatus</font></b></td>");
            str.Append("</tr>");
            foreach (NetworkAvailabality val in lstKPIData.platformServices.networkAvailabilities)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Region + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CurrentStatus + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            str.Append("<table border=`" + "1px" + "`b>");
            //Ticket Summary
            str.Append("<tr><b><u><font face=Arial Narrow size=" + "12px" + "> Ticket Summary </u> </b></tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>SummaryDetails</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>SummaryValue</font></b></td>");
            str.Append("</tr>");
            foreach (TicketSummary val in lstKPIData.platformServices.ticketSummaries)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.SummaryDetails + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.SummaryValue + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            str.Append("<table border=`" + "1px" + "`b>");
            //Alert Volume Summary
            str.Append("<tr><b><u><font face=Arial Narrow size=" + "12px" + "> Alert Volume</u> </b></tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>AlertVolumeTime</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>AlertVolume</font></b></td>");
            str.Append("</tr>");
            foreach (AlertVoulme val in lstKPIData.platformServices.alertVolumes)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.AlertVolumeTime.ToShortDateString() + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.AlertVolume + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            str.Append("<table border=`" + "1px" + "`b>");
            //Sentiance Health Summary
            str.Append("<tr><b><u><font face=Arial Narrow size=" + "12px" + "> Sentiance Health </u></b></tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>ProductName</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2></font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>CurrentStatus</font></b></td>");
            str.Append("</tr>");
            foreach (SentianceHealth val in lstKPIData.platformServices.sentianceHealths)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.ProductName + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.SLA + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.CurrentStatus + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            str.Append("<table border=`" + "1px" + "`b>");
            //Connected Device Summary Region
            str.Append("<tr><b><font face=Arial Narrow size=3> Connected Device Plots</b> </tr>");
            str.Append("<tr>");
            str.Append("<td><b><font face=Arial Narrow size=2>Lattitude</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>Longitude</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>Device Count</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>Intensity</font></b></td>");
            str.Append("<td><b><font face=Arial Narrow size=2>SBG</font></b></td>");
            str.Append("</tr>");
            foreach (ConnectedDevicePlot val in lstKPIData.lstConnectedDevicePlot)
            {
                str.Append("<tr>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Lattitude + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Longitude + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.DeviceCount + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.Intensity + "</font></td>");
                str.Append("<td><font face=Arial Narrow size=" + "14px" + ">" + val.SBG + "</font></td>");
                str.Append("</tr>");
            }
            str.Append("</table>"); str.Append("<br/>");

            #endregion

            return str;
        }
    }
}
