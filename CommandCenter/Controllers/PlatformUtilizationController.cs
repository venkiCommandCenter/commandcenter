
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace CommandCenter.Controllers
{
    [Route("api/PlatformUtilization")]
    public class PlatformUtilizationController : Controller
    {
        private IPlatformUtilization repository;        
        private readonly IHostingEnvironment _hostingEnvironment;

        private readonly ILogger _logger;
        IEnumerable<PlatformUtilization> platformUtilizations;
        
        public PlatformUtilizationController(IHostingEnvironment hostingEnvironment, IConfiguration confg, ILogger<PlatformUtilizationController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new PlatformUtilizationRepository(confg);
            _logger = logger;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<PlatformUtilization> Get()
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting Platform Utilization summary by API api/PlatformUtilization");
            try
            {
                platformUtilizations = repository.GetAll();
                _logger.LogInformation(LoggingEvents.GetItem, "Got the Platform Utilization summary by API api/PlatformUtilization");
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message);
            }
            return platformUtilizations;
        }

        [HttpGet]
        [Route("~/api/LiveProducts/{ProductId}")]
        public IEnumerable<PlatformUtilization> GetLiveProducts(int ProductId)
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting Platform Utilization summary based on {ProductId} API - /api/LiveProducts/ProductId", ProductId);
            try
            {
                platformUtilizations = repository.GetLiveProducts(ProductId);
                _logger.LogInformation(LoggingEvents.GetItem, "Getting Platform Utilization summary by API api/PlatformUtilization based on {ProductId}", ProductId);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.GetItemNotFound, "Error" + ex.StackTrace + "Message" + ex.Message, ProductId);
            }
            return platformUtilizations;
        }
    }
}
