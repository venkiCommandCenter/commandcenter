﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandCenter.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CommandCenter.Controllers
{
    [Route("api/UILogging")]
    public class UILoggingController : Controller
    {
        private readonly ILogger _logger;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UILoggingController(IHostingEnvironment hostingEnvironment, IConfiguration confg, ILogger<UILoggingController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
        }

        // GET: api/UILogging

        // To add the Loggin from UI Prospective, API to be called
        [HttpPost]
        public string Post([FromBody]UILogging logging)
        {
            try
            {
                if (logging.Parameters == "1")
                    _logger.LogInformation(LoggingEvents.UILoggingInformation, logging.Exception, logging.Message, logging.Parameters);
                else
                    _logger.LogError(LoggingEvents.UILoggingError, logging.Exception, logging.Message, logging.Parameters);
            }
            catch(Exception ex)
            {
                _logger.LogError(LoggingEvents.UILoggingError, "Exception Occured while logging error", "UILogging Error","Information");
            }

            return "Success";
        }

    }
}
