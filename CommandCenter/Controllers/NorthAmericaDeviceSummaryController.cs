using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Controllers
{

    [Route("api/NorthAmericaDeviceSummary")]
    public class NorthAmericaDeviceSummaryController : Controller
    {
        private INorthAmericaDeviceSummary repository;
        //OutagesDeviceSummaryRepository repository = new OutagesDeviceSummaryRepository();
        private readonly IHostingEnvironment _hostingEnvironment;

        public NorthAmericaDeviceSummaryController(IHostingEnvironment hostingEnvironment, IConfiguration confg)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new NorthAmericaDeviceSummaryRepository(confg);

        }
        // GET api/values
        [HttpGet]
        [Route("{GeoLocationID}")]
        public List<NorthAmericaDeviceSummary> Get(int GeoLocationID)
        {
            return repository.GetAll(GeoLocationID);
        }

        [HttpGet]
        [Route("{GeoLocationID}/{SBG}")]
        public List<NorthAmericaDeviceSummary> GetbySBG(int GeoLocationID, string sbg)
        {
            return repository.GetbySBG(GeoLocationID, sbg);
        }
    }
}
