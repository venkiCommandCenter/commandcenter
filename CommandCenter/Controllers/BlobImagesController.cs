using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;
using CommandCenter.Repository;
using CommandCenter.Model;


using Microsoft.AspNetCore.Hosting;

namespace CommandCenter.Controllers
{
    [Produces("application/json")]
    [Route("api/BlobImages")]
    public class BlobImagesController : Controller
    {
    private IBlobImages repository;
    private readonly IHostingEnvironment _hostingEnvironment;
    public BlobImagesController(IHostingEnvironment hostingEnvironment,IConfiguration confg)
    {
       _hostingEnvironment = hostingEnvironment;
      repository = new BlobImagesRepository(confg);
    }
    //[HttpGet]
    //public IEnumerable<BlobImages> Get()
    //{
    //  //IBlobImages images = new BlobImagesRepository();
    // return repository.GetImages();
      

    //}

    [HttpGet]
    public Task<IEnumerable<BlobImages>> Get()
    {
      //IBlobImages images = new BlobImagesRepository();
      return repository.GetImages();


    }
  }
}
