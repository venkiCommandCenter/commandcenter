using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CommandCenter.Model;
using CommandCenter.Repository;
using Microsoft.AspNetCore.Hosting;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Controllers
{
    [Route("api/NorthAmericaDevicePlot")]
    public class NorthAmericaDevicePlotController : Controller
    {
        private INorthAmericaDevicePlot repository;
        private readonly IHostingEnvironment _hostingEnvironment;
        internal INorthAmericaDevicePlot Repository { get => repository; set => repository = value; }

        public NorthAmericaDevicePlotController(IHostingEnvironment hostingEnvironment, IConfiguration confg)
        {
            _hostingEnvironment = hostingEnvironment;
            repository = new NorthAmericaDevicePlotRepository(confg);
        }
        // GET api/values
        [HttpGet]
        [Route("{GeoLocationID}")]
        public IEnumerable<NorthAmericaDevicePlot> Get(int GeoLocationID)
        {
            return repository.GetAll(GeoLocationID);
        }

        [HttpGet]
        [Route("{GeoLocationID}/{SBG}")]
        public IEnumerable<NorthAmericaDevicePlot> GetbySBG(int GeoLocationID, string sbg)
        {
            return repository.GetbySBG(GeoLocationID,sbg);
        }
    }
}
