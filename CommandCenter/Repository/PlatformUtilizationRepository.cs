using System;
using System.Data;
using System.Configuration;
using CommandCenter.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Repository
{
    public class PlatformUtilizationRepository : IPlatformUtilization
    {
        string connectionString;
        public PlatformUtilizationRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }

        public IEnumerable<PlatformUtilization> GetAll()
        {
            List<PlatformUtilization> lstPlatformUtilization = new List<PlatformUtilization>();

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetPlatformUtilization]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        PlatformUtilization platformUtilization = new PlatformUtilization();
                        platformUtilization.Id = reader.GetInt32(0);
                        platformUtilization.PlatformName = reader.GetString(1);
                        platformUtilization.MaxUtilization = reader.GetString(2);
                        platformUtilization.MinUtilization = reader.GetString(3);
                        platformUtilization.CurrentUtilization = reader.GetString(4);
                        platformUtilization.CurrentState = reader.GetString(5);
                        platformUtilization.PlatformUnit = reader[6] as string;
                        platformUtilization.PlanStatus = reader[7] as string;
                        lstPlatformUtilization.Add(platformUtilization);

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstPlatformUtilization.ToArray();
        }

        public IEnumerable<PlatformUtilization> GetLiveProducts(int productId)
        {
            List<PlatformUtilization> lstLiveProducts = new List<PlatformUtilization>();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetLiveProductsList]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ProductId", productId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        PlatformUtilization platformUtilization = new PlatformUtilization();
                        platformUtilization.Id = reader.GetInt32(0);
                        platformUtilization.SBGName = reader.GetString(1);
                        platformUtilization.SBGValue = reader.GetString(2);
                        lstLiveProducts.Add(platformUtilization);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstLiveProducts.ToArray();
        }
    }
}
