using System;
using System.Data;
using System.Configuration;
using CommandCenter.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Repository
{
    public class ConnectedDeviceSummaryRepository : IConnectedDeviceSummary
    {
        string connectionString;
        public ConnectedDeviceSummaryRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }   
        public List<ConnectedDeviceSummary> GetAll(int GeoLocationId)
        {
             List<ConnectedDeviceSummary> lstConnectedDeviceSummary = new List<ConnectedDeviceSummary>();                     
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetConnectedDeviceSummary]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GeoLocationID", GeoLocationId);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConnectedDeviceSummary connectedDeviceSummary = new ConnectedDeviceSummary();
                        connectedDeviceSummary.Id = reader.GetInt32(0);
                        connectedDeviceSummary.ConnectedDeviceLabel = reader.GetString(1);
                        connectedDeviceSummary.Value = reader.GetString(2);
                        connectedDeviceSummary.Units = reader[3] as string;
                        connectedDeviceSummary.Previous90Days = reader.GetString(6);
                        lstConnectedDeviceSummary.Add(connectedDeviceSummary);
                    }                                        
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstConnectedDeviceSummary;
        }
        public List<ConnectedDeviceSummary> GetbySBG(int GeoLocationID, string sbg)
        {
            List<ConnectedDeviceSummary> lstConnectedDeviceSummarybySBG = new List<ConnectedDeviceSummary>();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetConnectedDeviceSummarybySBG]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sbg == null)
                        sbg = "";
                    cmd.Parameters.AddWithValue("@GeoLocationID", GeoLocationID);
                    cmd.Parameters.AddWithValue("@SBG", sbg);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConnectedDeviceSummary connectedDeviceSummarybySBG = new ConnectedDeviceSummary();
                        connectedDeviceSummarybySBG.Id = reader.GetInt32(0);
                        connectedDeviceSummarybySBG.ConnectedDeviceLabel = reader.GetString(1);
                        connectedDeviceSummarybySBG.Value = reader.GetString(2);
                        connectedDeviceSummarybySBG.Units = reader[3] as string;
                        connectedDeviceSummarybySBG.SBG = reader.GetString(5);
                        connectedDeviceSummarybySBG.Previous90Days = reader.GetString(6);
                        //connectedDeviceSummarybySBG.GeoLocationId = reader.GetInt32(7);
                        lstConnectedDeviceSummarybySBG.Add(connectedDeviceSummarybySBG);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstConnectedDeviceSummarybySBG;
        }
    }
}
