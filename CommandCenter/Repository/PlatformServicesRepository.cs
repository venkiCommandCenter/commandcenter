﻿using System;
using System.Collections.Generic;
using System.Data;
using CommandCenter.Model;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;

namespace CommandCenter.Repository
{
    public class PlatformServicesRepository : IPlatformServices
    {
        string connectionString;
        public PlatformServicesRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }

        public PlatformServices GetPlatformServices()
        {            
            PlatformServices services = new PlatformServices();
            services.networkAvailabilities = new List<NetworkAvailabality>();
            services.ticketSummaries = new List<TicketSummary>();
            services.cloudServices = new List<CloudService>();
            services.alertVolumes = new List<AlertVoulme>();
            services.sentianceHealths = new List<SentianceHealth>();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetPlatformService]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        CloudService cloudService = new CloudService();
                        cloudService.Id = reader.GetInt32(0);
                        cloudService.CloudServiceName = reader.GetString(1);
                        cloudService.CloudServicePercentage = reader.GetString(2);
                        cloudService.CurrentStatus = reader.GetString(3);
                        services.cloudServices.Add(cloudService);

                    }
                    reader.NextResult();

                    while (reader.Read())
                    {   
                        NetworkAvailabality networkAvailability = new NetworkAvailabality();
                        networkAvailability.Id = reader.GetInt32(0);
                        networkAvailability.Region = reader.GetString(1);
                        networkAvailability.CurrentStatus = reader.GetString(2);
                        services.networkAvailabilities.Add(networkAvailability);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        TicketSummary ticketSummary = new TicketSummary();
                        ticketSummary.Id = reader.GetInt32(0);
                        ticketSummary.SummaryDetails = reader.GetString(1);
                        ticketSummary.SummaryValue = reader.GetString(2);
                        services.ticketSummaries.Add(ticketSummary);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        AlertVoulme alertVolume = new AlertVoulme();
                        alertVolume.AlertVolumeID = reader.GetInt32(0);
                        alertVolume.AlertVolumeTime = reader.GetDateTime(1);
                        alertVolume.AlertVolume = reader.GetInt32(2);
                        services.alertVolumes.Add(alertVolume);
                    }

                    reader.NextResult();

                    while (reader.Read())
                    {
                        SentianceHealth sentianceHealth = new SentianceHealth();
                        sentianceHealth.Id = reader.GetInt32(0);
                        sentianceHealth.ProductName = reader.GetString(1);
                        sentianceHealth.SLA = reader.GetString(2);
                        sentianceHealth.CurrentStatus = reader.GetString(3);
                        services.sentianceHealths.Add(sentianceHealth);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return services;
        }
    }
}
