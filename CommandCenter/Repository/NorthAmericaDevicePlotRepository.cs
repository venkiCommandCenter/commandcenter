using System;
using System.Data;
using System.Configuration;
using CommandCenter.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Repository
{
    public class NorthAmericaDevicePlotRepository : INorthAmericaDevicePlot
    {
        string connectionString;
        public NorthAmericaDevicePlotRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }
        public IEnumerable<NorthAmericaDevicePlot> GetAll(int GeoLocationID)
        {
            List<NorthAmericaDevicePlot> lstOutagesDevicePlot = new List<NorthAmericaDevicePlot>();

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetAllNorthAmericaDevicePlot]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@GeoLocationID", SqlDbType.Int).Value = GeoLocationID;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NorthAmericaDevicePlot outagesDevicePlot = new NorthAmericaDevicePlot();
                        outagesDevicePlot.Id = reader.GetInt32(0);
                        outagesDevicePlot.Lattitude = reader.GetString(1);
                        outagesDevicePlot.Longitude = reader.GetString(2);
                        outagesDevicePlot.DeviceCount = reader.GetString(3);
                        outagesDevicePlot.Intensity = reader.GetString(4);
                        outagesDevicePlot.GeoLocation = reader.GetString(5);
                        lstOutagesDevicePlot.Add(outagesDevicePlot);
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstOutagesDevicePlot.ToArray();
        }

        public IEnumerable<NorthAmericaDevicePlot> GetbySBG(int GeoLocationID, string sbg)
        {
            List<NorthAmericaDevicePlot> lstOutagesDevicePlot = new List<NorthAmericaDevicePlot>();

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetNorthAmericaDevicePlotbySBG]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@GeoLocationID", GeoLocationID);
                    cmd.Parameters.AddWithValue("@SBG", sbg);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NorthAmericaDevicePlot outagesDevicePlot = new NorthAmericaDevicePlot();
                        outagesDevicePlot.Id = reader.GetInt32(0);
                        outagesDevicePlot.Lattitude = reader.GetString(1);
                        outagesDevicePlot.Longitude = reader.GetString(2);
                        outagesDevicePlot.DeviceCount = reader.GetString(3);
                        outagesDevicePlot.Intensity = reader.GetString(4);
                        outagesDevicePlot.GeoLocation = reader.GetString(5);
                        outagesDevicePlot.SBG = reader.GetString(7);
                        lstOutagesDevicePlot.Add(outagesDevicePlot);
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstOutagesDevicePlot.ToArray();
        }
    }
}
