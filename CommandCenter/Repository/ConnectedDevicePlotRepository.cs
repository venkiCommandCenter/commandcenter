using CommandCenter.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;

namespace CommandCenter.Repository
{
    public class ConnectedDevicePlotRepository : IConnectedDevicePlot
    {
        string connectionString;

        public ConnectedDevicePlotRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }
        /// <summary>
        /// Get Connected device plot data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConnectedDevicePlot> GetAll()
        {

            List<ConnectedDevicePlot> connectedDevicePlots = new List<ConnectedDevicePlot>();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "GETALLCONNECTEDDEVICESPLOT";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConnectedDevicePlot connectedDevicePlot = new ConnectedDevicePlot();
                        //ConnectedDevicePlot connectedDevicePlot = new ConnectedDevicePlot()
                        //{
                        // connectedDevicePlot.Id = reader.GetInt32(0);
                        connectedDevicePlot.Lattitude = reader.GetString(0);
                        connectedDevicePlot.Longitude = reader.GetString(1);
                        connectedDevicePlot.DeviceCount = reader.GetString(2);
                        connectedDevicePlot.Intensity = reader.GetString(3);
                        //connectedDevicePlot.GeoLocation = reader.GetString(5);
                        //};
                        connectedDevicePlots.Add(connectedDevicePlot);
                    }
                    con.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return connectedDevicePlots.ToArray();
        }

        public IEnumerable<ConnectedDevicePlot> GetbySBG(string sbg)
        {
            List<ConnectedDevicePlot> connectedDevicePlots = new List<ConnectedDevicePlot>();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "GETCONNECTEDDEVICESPLOTBYSBG";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (sbg == null)
                        sbg = "";
                    cmd.Parameters.AddWithValue("@SBG", sbg);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ConnectedDevicePlot connectedDevicePlot = new ConnectedDevicePlot();
                        connectedDevicePlot.Lattitude = reader.GetString(0);
                        connectedDevicePlot.Longitude = reader.GetString(1);
                        connectedDevicePlot.DeviceCount = reader.GetString(2);
                        connectedDevicePlot.Intensity = reader.GetString(3);
                        connectedDevicePlot.SBG = reader[5] as string;
                        connectedDevicePlots.Add(connectedDevicePlot);
                    }
                    con.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return connectedDevicePlots.ToArray();

        }

        /// <summary>
        /// To upload the data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// /// <param name="tableName"></param>
        /// <returns></returns>
        public bool Upload<T>(List<T> data, string tableName)
        {
            try
            {
                //Delete all the records in the database
                //   Delete();                
                var dt = ConvertToDataTable(data);
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    SqlBulkCopy objBulk = new SqlBulkCopy(conn);
                    objBulk.DestinationTableName = tableName;
                    foreach (DataColumn col in dt.Columns)
                    {
                        objBulk.ColumnMappings.Add(sourceColumn: col.ColumnName, destinationColumn: col.ColumnName);
                    }
                    conn.Open();
                    objBulk.WriteToServer(dt);
                    conn.Close();
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public void Delete()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    string sp = "DeleteRecords";
                    SqlCommand cmd = new SqlCommand(sp, conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        static DataTable ConvertToDataTable<T>(IList<T> list)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in list)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        
    }
}
