using System;
using System.Data;
using System.Configuration;
using CommandCenter.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Repository
{
    public class NorthAmericaDeviceSummaryRepository : INorthAmericaDeviceSummary
    {
        string connectionString;
        public NorthAmericaDeviceSummaryRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }
        public List<NorthAmericaDeviceSummary> GetAll(int GeoLocationID)
        {
            List<NorthAmericaDeviceSummary> lstOutagesDevicePlot = new List<NorthAmericaDeviceSummary>();
            
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetNorthAmericaDeviceSummary]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@GeoLocationID", SqlDbType.Int).Value = GeoLocationID;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NorthAmericaDeviceSummary outagesDeviceSummary = new NorthAmericaDeviceSummary
                        {
                            Id = reader.GetInt32(0),
                            Label = reader.GetString(1),
                            Value = reader.GetString(2),
                            Unit = reader[3] as string,
                            GeoLocation = reader.GetString(4),
                            Previous90Days = reader[6] as string
                            
                        };
                        lstOutagesDevicePlot.Add(outagesDeviceSummary);
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstOutagesDevicePlot;
        }
        public List<NorthAmericaDeviceSummary> GetbySBG(int GeoLocationID, string sbg)
        {
            List<NorthAmericaDeviceSummary> lstOutagesDevicePlotbySBG = new List<NorthAmericaDeviceSummary>();

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetNorthAmericaDeviceSummarybySBG]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("@GeoLocationID", SqlDbType.Int).Value = GeoLocationID;
                    cmd.Parameters.AddWithValue("@GeoLocationID", GeoLocationID);
                    cmd.Parameters.AddWithValue("@SBG", sbg);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        NorthAmericaDeviceSummary outagesDeviceSummarybySBG = new NorthAmericaDeviceSummary
                        {
                            Id = reader.GetInt32(0),
                            Label = reader.GetString(1),
                            Value = reader.GetString(2),
                            Unit = reader[3] as string,
                            GeoLocation = reader.GetString(4),
                            SBG = reader.GetString(5),
                            Previous90Days = reader[6] as string
                        };
                        lstOutagesDevicePlotbySBG.Add(outagesDeviceSummarybySBG);
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return lstOutagesDevicePlotbySBG;
        }
    }
}
