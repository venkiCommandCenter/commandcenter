using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.ComponentModel;
using Microsoft.Extensions.Configuration;
using CommandCenter.Interfaces;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using CommandCenter.Model;

namespace CommandCenter.Repository
{
  public class BlobImagesRepository : IBlobImages
  {
    string connectionString;
    public BlobImagesRepository(IConfiguration confg)
    {
      connectionString = confg["ConnectionStringsImages:DefaultConnection"];
    }
    public async  Task<IEnumerable<BlobImages>> GetImages()
    {
            List<BlobImages> lstBlobImages = new List<BlobImages>();
            string userName = System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"];
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("command-center-blob");

            BlobContinuationToken continuationToken = null;

            BlobContainerPermissions containerPermissions = new BlobContainerPermissions();

            containerPermissions.PublicAccess = BlobContainerPublicAccessType.Container;

            var response = await container.ListBlobsSegmentedAsync("DarkThemeCenterContainer", true,
                                 new BlobListingDetails(), null, continuationToken, null, null);
            continuationToken = response.ContinuationToken;

            foreach (IListBlobItem blobItem in response.Results)
            {

                BlobImages image = new BlobImages();
                image.BlobImageName = ((CloudBlockBlob)blobItem).Name;
                image.BlobImageURL = blobItem.Uri.ToString();

                lstBlobImages.Add(image);
            }
            return lstBlobImages;
        }
    }  
}
