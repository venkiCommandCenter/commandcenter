﻿using CommandCenter.Interfaces;
using CommandCenter.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Repository
{
    public class DownloadKPIDataRepository : IDownloadKPIData
    {
        private IConnectedDeviceSummary repository;
        private IPlatformUtilization platformUtilization;
        private IPlatformServices platformServices;
        private IConnectedDevicePlot connectedDevicePlot;

        string connectionString;
        public DownloadKPIDataRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
            repository = new ConnectedDeviceSummaryRepository(confg);
            platformUtilization = new PlatformUtilizationRepository(confg);
            platformServices = new PlatformServicesRepository(confg);
            connectedDevicePlot = new ConnectedDevicePlotRepository(confg);
        }


        public DownloadKPIData GetReport(int GeoLocationId, string sbg = null)
        {
            DownloadKPIData kpiData = new DownloadKPIData();

            List<ConnectedDeviceSummary> lstConnectedDeviceSummarybySBG = new List<ConnectedDeviceSummary>();
            lstConnectedDeviceSummarybySBG = repository.GetbySBG(GeoLocationId, sbg);
            kpiData.lstConnectedDeviceSummary = lstConnectedDeviceSummarybySBG;

            List<PlatformUtilization> lstPlatformUtilizations = new List<PlatformUtilization>();
            if(platformUtilization.GetAll().Count() > 0)
                lstPlatformUtilizations = platformUtilization.GetAll().ToList();
            kpiData.lstPlatformUtilization = lstPlatformUtilizations;

            PlatformServices lstPlatformServices = new PlatformServices();
            lstPlatformServices = platformServices.GetPlatformServices();
            kpiData.platformServices = lstPlatformServices;

            List<ConnectedDevicePlot> lstConnectedDevicePlots = new List<ConnectedDevicePlot>();
            if(connectedDevicePlot.GetbySBG(sbg).Count() > 0)
                lstConnectedDevicePlots = connectedDevicePlot.GetbySBG(sbg).ToList();
            kpiData.lstConnectedDevicePlot = lstConnectedDevicePlots;

            return kpiData;

            #region KPI Data Code
            //List<DownloadKPIData> lstKPIData = new List<DownloadKPIData>();
            //try
            //{
            //    using (SqlConnection con = new SqlConnection(connectionString))
            //    {
            //        string sp = "[dbo].[GetConnectedDeviceSummary]";
            //        SqlCommand cmd = new SqlCommand(sp, con);
            //        con.Open();
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        SqlDataReader reader = cmd.ExecuteReader();
            //        while (reader.Read())
            //        {
            //            DownloadKPIData connectedDeviceSummary = new DownloadKPIData();
            //            connectedDeviceSummary.Id = reader.GetInt32(0);
            //            connectedDeviceSummary.ConnectedDeviceLabel = reader.GetString(1);
            //            connectedDeviceSummary.Value = reader.GetString(2);
            //            connectedDeviceSummary.Units = reader[3] as string;
            //            lstKPIData.Add(connectedDeviceSummary);
            //        }
            //    }
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
            //return lstKPIData; 
            #endregion
        }
    }
}
