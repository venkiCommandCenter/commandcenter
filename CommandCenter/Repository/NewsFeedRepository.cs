﻿using System;
using System.Data;
using System.Configuration;
using CommandCenter.Model;
using System.Data.SqlClient;
using System.Collections.Generic;
using CommandCenter.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommandCenter.Repository
{
    public class NewsFeedRepository : INewsFeed
    {
        string connectionString;
        public NewsFeedRepository(IConfiguration confg)
        {
            connectionString = confg["ConnectionStrings:DefaultConnection"];
        }

        public NewsFeed GetAll()
        {
            NewsFeed newsFeed = new NewsFeed();
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string sp = "[dbo].[GetNewsFeedAlert]";
                    SqlCommand cmd = new SqlCommand(sp, con);
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = cmd.ExecuteReader();                   

                    while (reader.Read())
                    {
                       
                        newsFeed.NewsId = reader.GetInt32(0);
                        newsFeed.CurrentNewsFeed = reader.GetString(1);
                        newsFeed.NextNewsFeed = reader.GetString(2);
                        newsFeed.TomorrowNewsFeed = reader.GetString(3);                      
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return newsFeed;
        }
     }
}
