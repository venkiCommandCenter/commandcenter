using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace CommandCenter.Model
{
    public class BlobImages
    {
    public string BlobImageName { get; set; }
    public string BlobImageURL { get; set; }
    public MemoryStream ImageStream { get; set; }
  }
}
