﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class NorthAmericaDeviceSummary
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public string Unit { get; set; }
        public string GeoLocation { get; set; }
        public string SBG { get; set; }
        public string Previous90Days { get; set; }
    }
}
