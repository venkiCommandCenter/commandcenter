﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class DownloadKPIData
    {
        public List<ConnectedDeviceSummary> lstConnectedDeviceSummary { get; set; }
        public List<PlatformUtilization> lstPlatformUtilization { get; set; }
        public PlatformServices platformServices { get; set; }
        public List<ConnectedDevicePlot> lstConnectedDevicePlot { get; set; }

    }
}
