﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class NewsFeed
    {
        public int NewsId { get; set; }
        public string CurrentNewsFeed { get; set; }
        public string NextNewsFeed { get; set; }
        public string TomorrowNewsFeed { get; set; }
    }
}
