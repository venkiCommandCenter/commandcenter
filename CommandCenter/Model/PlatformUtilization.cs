﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class PlatformUtilization
    {
        public int Id { get; set; }
        public string PlatformName { get; set; }
        public string MaxUtilization { get; set; }
        public string MinUtilization { get; set; }
        public string CurrentUtilization { get; set; }
        public string CurrentState { get; set; }
        public string PlatformUnit { get; set; }
        public string PlanStatus { get; set; }
        public string SBGName { get; set; }
        public string SBGValue { get; set; }


    }
}
