﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class TicketSummary
    {
        public int Id { get; set; }
        public string SummaryDetails { get; set; }
        public string SummaryValue { get; set; }
    }
}
