﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class NetworkAvailabality
    {
        public int Id { get; set; }
        public string Region { get; set; }

        public string CurrentStatus { get; set; }
    }
}
