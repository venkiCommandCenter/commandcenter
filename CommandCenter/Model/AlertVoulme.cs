﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class AlertVoulme
    {
        public int AlertVolumeID { get; set; }
        public DateTime AlertVolumeTime { get; set; }
        public int AlertVolume { get; set; }
    }
}
