﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class PlatformServices
    {
        public List<CloudService> cloudServices { get; set; }

        public List<NetworkAvailabality> networkAvailabilities { get; set; }

        public List<TicketSummary> ticketSummaries { get; set; }

        public List<AlertVoulme> alertVolumes { get; set; }

        public List<SentianceHealth> sentianceHealths { get; set; }
    }
}
