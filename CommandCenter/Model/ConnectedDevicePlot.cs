﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class ConnectedDevicePlot
    {
        public int Id { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public string DeviceCount { get; set; }
        public string Intensity { get; set; }
        public string GeoLocation { get; set; }
        public string SBG { get; set; }
    }
}
