﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class CloudService
    {
        public int Id { get; set; }
        public string CloudServiceName { get; set; }
        public string CloudServicePercentage { get; set; }
        public string CurrentStatus { get; set; }
    }
}
