﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandCenter.Model
{
    public class ConnectedDeviceSummary
    {
        public int Id { get; set; }
        public string ConnectedDeviceLabel { get; set; }
        public string Value { get; set; }
        public string Units { get; set; }
        public string SBG { get; set; }
        public string Previous90Days { get; set; }
        public int GeoLocationId { get; set; }
        
    }
}
