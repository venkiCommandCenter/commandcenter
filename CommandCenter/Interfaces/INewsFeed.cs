﻿using CommandCenter.Model;
using System.Collections.Generic;

namespace CommandCenter.Interfaces
{
    interface INewsFeed
    {
        NewsFeed GetAll();
    }
}
