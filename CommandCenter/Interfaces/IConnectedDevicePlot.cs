﻿using CommandCenter.Model;
using System.Collections.Generic;

namespace CommandCenter.Interfaces
{
    interface IConnectedDevicePlot
    {
        IEnumerable<ConnectedDevicePlot> GetAll();
        IEnumerable<ConnectedDevicePlot> GetbySBG(string sbg);
        bool Upload<T>(List<T> data, string tableName);
        void Delete();
        
    }
}
