﻿using CommandCenter.Model;

namespace CommandCenter.Interfaces
{
    interface IPlatformServices
    {
        PlatformServices GetPlatformServices();
    }
}
