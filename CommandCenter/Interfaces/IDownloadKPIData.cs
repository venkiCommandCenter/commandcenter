﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandCenter.Model;

namespace CommandCenter.Interfaces
{
    interface IDownloadKPIData
    {
        DownloadKPIData GetReport(int GeoLocationId, string sbg = null);
    }
}
