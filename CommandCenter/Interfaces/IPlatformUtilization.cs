using CommandCenter.Model;
using System.Collections.Generic;

namespace CommandCenter.Interfaces
{
    interface IPlatformUtilization
    {
        IEnumerable<PlatformUtilization> GetAll();

        IEnumerable<PlatformUtilization> GetLiveProducts(int productId);
    }
}
