using CommandCenter.Model;
using System.Collections.Generic;

namespace CommandCenter.Interfaces
{
    interface INorthAmericaDevicePlot
    {
        IEnumerable<NorthAmericaDevicePlot> GetAll(int GeoLocationID);
        IEnumerable<NorthAmericaDevicePlot> GetbySBG(int GeoLocationID, string sbg);
    }
}
