using CommandCenter.Model;
using System.Collections.Generic;

namespace CommandCenter.Interfaces
{
    interface IConnectedDeviceSummary
    {
        List<ConnectedDeviceSummary> GetAll(int GeoLocationId);
        List<ConnectedDeviceSummary> GetbySBG(int GeoLocationId, string sbg);
    }
}
