using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommandCenter.Model;

namespace CommandCenter.Interfaces
{
    interface IBlobImages
    {
    //IEnumerable<BlobImages> GetImages();
   Task<IEnumerable<BlobImages>> GetImages();
  }
}
