using CommandCenter.Model;
using System.Collections.Generic;

namespace CommandCenter.Interfaces
{
    interface INorthAmericaDeviceSummary
    {
        List<NorthAmericaDeviceSummary> GetAll(int GeoLocationID);
        List<NorthAmericaDeviceSummary> GetbySBG(int GeoLocationID, string sbg);
    }
}
