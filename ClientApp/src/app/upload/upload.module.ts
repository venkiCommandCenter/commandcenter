import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';

// routes
import { UploadRouteModule } from './upload.route';

// components
import { UploadDataComponent } from './components/upload-data.component';

@NgModule({
  declarations: [
    UploadDataComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    UploadRouteModule
  ],
  providers: [HttpModule],
  exports: []
})
export class UploadModule { }
