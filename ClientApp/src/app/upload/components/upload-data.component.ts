import { NgModule, Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Constants } from '../../shared/constants';

@Component({
  selector: 'app-upload',
  templateUrl: './upload-data.component.html',
  styleUrls: ['./upload-data.component.css']
})

export class UploadDataComponent implements OnInit {
  public uploadStatus: boolean = false;
  public uploadCD: boolean = false;
  public baseURL = Constants.baseURL;
  constructor(private _http: HttpClient) { }

  ngOnInit() {
  }

  fileChange(event: any) {
    this.uploadCD = true;
    this.uploadStatus = true;
    console.log("i am loading file");
    debugger;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0)
    {
      let file: File = fileList[0];
      let formData = new FormData();
      formData.append("files", file);
      //let headers = new Headers();
      //headers.append('Content-Type', 'json');
      //headers.append('Accept', 'application/json');
      //let options = new RequestOptions({ headers: headers });
      let apiURL = this.baseURL + 'api/ConnectedDevicePlot/Upload';
      this._http.post(apiURL, formData)
        .subscribe(
        data => {         
          this.uploadCD = false;
        },
        error => console.log('error')
        )
    }
  }

}
