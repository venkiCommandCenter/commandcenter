import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UploadDataComponent } from './components/upload-data.component';

const uploadRoutes: Routes = [
    {
        path: '',
        redirectTo: 'upload-data',
        pathMatch: 'full'
    },
    {
        path: 'upload-data',
        component: UploadDataComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(uploadRoutes)
    ], exports: [
        RouterModule
    ]
})

export class UploadRouteModule {}
