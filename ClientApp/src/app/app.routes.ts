import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {
        path: 'globe',
        loadChildren: './globe/globe.module#GlobeModule',
        data: {
            globe: true
        }
    },
    {
        path: 'platform',
        loadChildren: './platform/platform.module#PlatformModule'
    },
    {
        path: 'upload',
        loadChildren: './upload/upload.module#UploadModule'
    },
    {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'mobile',
        loadChildren: './mobile/mobile.module#MobileModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ], exports: [
        RouterModule
    ]
})

export class AppRouteModule {}
