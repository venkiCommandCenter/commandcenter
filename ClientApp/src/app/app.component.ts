import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild('popup') popup: ElementRef;
  msg: string;

  constructor(
    private router: Router,
    private deviceService: DeviceDetectorService
  ) { }

  ngOnInit() {
    const deviceInfo = this.deviceService.getDeviceInfo();
    if (deviceInfo.device === 'android' || deviceInfo.device === 'iphone') {
      this.router.navigate(['/mobile']);
      if (deviceInfo.device === 'iphone' && deviceInfo.browser !== 'safari') {
        this.msg = 'Please open in Safari for better experience..';
        this.openModal();
      }
    } else if (this.isBrowserZoomed()) {
      this.msg = 'Please set your browser zoom level to 100% for better experience..';
      this.openModal();
    }
  }

  openModal() {
    this.popup.nativeElement.className = 'modal fade in';
  }

  closeModal() {
    this.popup.nativeElement.className = 'modal fade';
  }

  isBrowserZoomed(): boolean {
    const screenCssPixelRatio = (window.outerWidth - 8) / window.innerWidth;
    return (screenCssPixelRatio < .98 || screenCssPixelRatio > 1.02);
  }
}
