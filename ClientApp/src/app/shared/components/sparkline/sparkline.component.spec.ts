import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SparklineComponent } from './sparkline.component';

describe('SparklineComponent', () => {
  let component: SparklineComponent;
  let fixture: ComponentFixture<SparklineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SparklineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SparklineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have defined 'margin'`, () => {
    expect(component.margin).toBeDefined();
  });

  it(`should have defined 'bisectDate'`, () => {
    expect(component.bisectDate).toBeDefined();
  });

  it(`should have called 'init()' with no argument`, () => {
    spyOn(component, 'init');
    component.init();
    expect(component.init).toBeDefined();
    expect(component.init).toHaveBeenCalled();
    expect(component.init).toHaveBeenCalledWith();
  });

  it(`should have called 'render()' with no argument`, () => {
    spyOn(component, 'render');
    component.render();
    expect(component.render).toBeDefined();
    expect(component.render).toHaveBeenCalled();
    expect(component.render).toHaveBeenCalledWith();
  });
});
