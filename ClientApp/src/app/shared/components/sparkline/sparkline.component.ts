import { Component, OnInit, Input, ElementRef, ViewEncapsulation } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-sparkline',
  template: '',
  styleUrls: ['./sparkline.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SparklineComponent implements OnInit {

  svg: any;
  xScale: any;
  yScale: any;
  xAxis: any;
  valueLine: any;
  margin = {
    top: 10,
    right: 25,
    bottom: 40,
    left: 20
  };
  chartData: any;
  animateTime = 2000;
  bisectDate = d3.bisector((d) => d.alertVolumeTime).left;
  @Input('width') width: any;
  @Input('height') height: any;
  @Input()
  set data(data: any) {
    if (data && data.length) {
      this.chartData = data;
      this.chartData.forEach(d => {
        d.alertVolumeTime = Date.parse(d.alertVolumeTime);
      });
      this.init();
    }
  }
  @Input()
  set options(option: any) {
    if (option) {
      // do something
    }
  }

  constructor(
    private element: ElementRef
  ) { }

  ngOnInit() {
    window.addEventListener('resize', () => {
      if (this.chartData) {
        this.render();
      }
    });
  }

  init() {
    const data = this.chartData;
    if (this.svg) {
      this.svg.selectAll('*').remove();
    } else {
      this.svg = d3.select(this.element.nativeElement).append('svg');
    }
    // gradiant effect
    const defs = this.svg.append('defs');
    const gradient = defs.append('linearGradient')
      .attr('id', 'svgGradient')
      .attr('x1', '0%')
      .attr('x2', '100%');
    gradient.append('stop')
      .attr('class', 'start')
      .attr('offset', '0%')
      .attr('stop-color', '#1792E5')
      .attr('stop-opacity', 0);
    gradient.append('stop')
      .attr('class', 'end')
      .attr('offset', '100%')
      .attr('stop-color', '#1792E5')
      .attr('stop-opacity', 1);

    this.svg.append('g')
      .classed('chart-layer', true)
      .append('path')
      .datum(data)
      .attr('stroke', 'url(#svgGradient)')
      .classed('line', true);
    this.svg.append('g')
      .classed('x-axis', true);
    this.svg.append('circle')
      .classed('end-pointer', true);
    this.svg.append('text')
      .classed('end-pointer-text', true);

    const xExtent = d3.extent(data, (d) => {
      return d.alertVolumeTime;
    });

    const yExtent = d3.extent(data, (d) => {
      return d.alertVolume;
    });

    this.xScale = d3.scaleLinear().domain(xExtent);
    this.yScale = d3.scaleLinear().domain(yExtent);

    this.xAxis = d3.axisBottom()
      .scale(this.xScale)
      .ticks(3)
      .tickFormat(d3.timeFormat('%b %d'));

    this.valueLine = d3.line().x((d) => {
      return this.xScale(d.alertVolumeTime);
    }).y((d) => {
      return this.yScale(d.alertVolume);
    }).curve(d3.curveCardinal);

    this.render();
  }

  render() {
    const data = this.chartData;
    // update svg elements to new dimensions
    this.svg.attr('width', this.width)
      .attr('height', this.height);

    const dimension = this.svg.node().getBoundingClientRect();
    const width = dimension.width;
    const height = dimension.height;

    this.xScale.rangeRound([this.margin.left, width - (this.margin.right + this.margin.left)]);
    this.yScale.rangeRound([height - (this.margin.top + this.margin.bottom), this.margin.top]);

    const xLength = this.xScale(data[data.length - 1].alertVolumeTime);
    const yLength = this.yScale(data[data.length - 1].alertVolume);

    // update the axis and line
    this.xAxis.scale(this.xScale);

    this.svg.select('.x-axis')
      .attr('transform', `translate(0,${height - 30})`)
      .call(this.xAxis);

    this.svg.select('.end-pointer')
      .attr('cx', xLength)
      .attr('cy', yLength)
      .attr('r', 0)
      .transition()
        .duration(this.animateTime)
        .attr('r', 5);

    const finalPoint = data[data.length - 1].alertVolume;
    let pointerTxtY = 8;
    const pointerText = this.svg.select('.end-pointer-text');
    pointerText
      .text(finalPoint)
      .style('font-size', '0')
      .transition()
        .duration(this.animateTime)
        .style('font-size', () => {
          if (finalPoint.toString().length <= 2) {
            return '25px';
          } else if (finalPoint.toString().length <= 4) {
            pointerTxtY = 5;
            return '12px';
          } else {
            pointerTxtY = 3;
            return '8px';
          }
        });

    pointerText
      .attr('transform', `translate(${xLength + 10}, ${yLength + pointerTxtY})`);

    const path = this.svg.select('.line');
    path.attr('d', this.valueLine)
      .attr('stroke-dasharray', 2000)
      .attr('stroke-dashoffset', 2000)
      .transition()
        .duration(this.animateTime)
        .attr('stroke-dashoffset', 0);

    // tooltip
    const focus = this.svg.append('g')
      .attr('class', 'focus')
      .style('display', 'none');

    focus.append('circle')
      .attr('r', 5);

    focus.append('text')
      .attr('dy', '1.5em');

    this.svg.append('rect')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')')
      .attr('class', 'overlay')
      .attr('width', width)
      .attr('height', height)
      .on('mouseover', function() { focus.style('display', null); })
      .on('mouseout', function() { focus.style('display', 'none'); })
      .on('mousemove', mousemove);

    const _this = this;
    function mousemove() {
      const x0 = _this.xScale.invert(d3.mouse(this)[0]),
          i = _this.bisectDate(data, x0, 1),
          d0 = data[i - 1],
          d1 = data[i],
          d = d1 && (x0 - d0.alertVolumeTime > d1.alertVolumeTime - x0) ? d1 : d0;
      focus.attr('transform', 'translate(' + _this.xScale(d.alertVolumeTime) + ',' + _this.yScale(d.alertVolume) + ')');
      focus.select('text').text(function() { return d.alertVolume; });
    }
  }

}
