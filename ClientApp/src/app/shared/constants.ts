export class Constants {
    // static baseURL = 'https://commandcenterhoneywellapi.azurewebsites.net/';
    // static baseURL = 'http://localhost:64744/';
    static baseURL = 'https://commandcenter2honeywell-api.azurewebsites.net/';
    static intevalTimer = 20000;
    static intervalGlobeData = 2700000;
    static geoBaseURL = 'https://nominatim.openstreetmap.org/reverse?';
}
