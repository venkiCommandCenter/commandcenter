import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountupDirective } from './directives/countup.directive';
import { GlobeDirective } from './directives/globe.directive';
import { SparklineComponent } from './components/sparkline/sparkline.component';
import { LoggerService } from './services/logger.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CountupDirective,
    GlobeDirective,
    SparklineComponent
  ],
  exports: [
    CountupDirective,
    GlobeDirective,
    SparklineComponent
  ],
  providers: [
    LoggerService
  ]
})
export class SharedModule { }
