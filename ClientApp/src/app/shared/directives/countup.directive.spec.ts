import { inject, TestBed } from '@angular/core/testing';
import { ElementRef } from '@angular/core';
import { CountupDirective } from './countup.directive';

export class MockElementRef extends ElementRef {
  constructor() { super(null); }
}

describe('CountupDirective', () => {
  let directive: CountupDirective;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MockElementRef]
    });
  });

  beforeEach(inject([MockElementRef], (elementRef) => {
    directive = new CountupDirective(elementRef);
  }));

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it(`should have called 'createCountUp()' with all possible arguments`, () => {
    spyOn(directive, 'createCountUp');
    directive.createCountUp(10, 10, 10, 10);
    directive.createCountUp(10, 10, 10, null);
    directive.createCountUp(10, 10, null, null);
    directive.createCountUp(10, null, null, null);
    directive.createCountUp(null, null, null, null);
    expect(directive.createCountUp).toBeDefined();
    expect(directive.createCountUp).toHaveBeenCalled();
    expect(directive.createCountUp).toHaveBeenCalledWith(10, 10, 10, 10);
    expect(directive.createCountUp).toHaveBeenCalledWith(10, 10, 10, null);
    expect(directive.createCountUp).toHaveBeenCalledWith(10, 10, null, null);
    expect(directive.createCountUp).toHaveBeenCalledWith(10, null, null, null);
    expect(directive.createCountUp).toHaveBeenCalledWith(null, null, null, null);
  });

  it(`should have called 'animate()' with no arguments`, () => {
    spyOn(directive, 'animate');
    directive.animate();
    expect(directive.animate).toBeDefined();
    expect(directive.animate).toHaveBeenCalled();
    expect(directive.animate).toHaveBeenCalledWith();
  });
});
