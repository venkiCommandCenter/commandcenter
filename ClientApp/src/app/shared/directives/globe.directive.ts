import { Directive, OnInit, ElementRef, Input, Output, EventEmitter, Renderer2 } from '@angular/core';
import { GeocoadingService } from '../services/geocoading.service';
import * as AlloyTouch from 'alloytouch';
import * as _ from 'lodash';

declare let THREE: any;

@Directive({
  selector: '[appGlobe]',
  providers: [GeocoadingService]
})
export class GlobeDirective implements OnInit {
  container: any;
  widthScaleFactor = 0;
  maxWidthScaleValue = 400; // to change the aspect ratio for big screen streached view
  isBigScreen: boolean;
  isMobileScreen: boolean;
  imgDir: string;
  shaders: any;
  camera: any;
  scene: any;
  webglRenderer: any;
  w: number;
  h: number;
  mesh: any;
  overRenderer: any;
  curZoomSpeed = 0;
  zoomSpeed = 50;
  zoomFactor = 1300; // increase the value to reduce the initial globe size
  mouse: any = { x: 0, y: 0 };
  mouseOnDown: any = { x: 0, y: 0 };
  rotation: any = { x: 0, y: 0 };
  target: any = { x: Math.PI * 3 / 2, y: Math.PI / 6.0 };
  targetOnDown: any = { x: 0, y: 0 };
  distance = 100000;
  distanceTarget = 100000;
  padding = 40;
  PI_HALF = Math.PI / 2;
  doRotate = true;  // Determines globe rotation
  isRevolving = false;
  globeData: Array<any>;

  mouseMoveListener: any;
  mouseUpListener: any;
  mouseOutListener: any;

  fieldMap: any;
  colorDef: any;
  hoverColor = 0xffffff;
  isHovered = false; // Determined if any spike is hovered immediately
  currentSelected: any;
  currentHovered: any;
  popTimeout: any;
  spikeName = 'spike';
  popName = 'pop-element';
  popoverName = 'popover';
  popoverHeight = 70;
  popoverTextSize = 15;
  scaleVector = new THREE.Vector3();
  initialScale = 138;
  isGlobeLoaded = false;

  @Input() options: any;
  @Input()
  set data(data: any) {
    if (data) {
      this.globeData = data;
      this.addData(data);
    }
  }

  @Output() private select: EventEmitter<any> = new EventEmitter();

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private geocoadingService: GeocoadingService
  ) {
    this.container = elementRef.nativeElement;
  }

  ngOnInit() {
    this.setShader();
    this.initGlobe();
    this.enableTouch();
  }

  setShader() {
    this.shaders = {
      'earth': {
        uniforms: {
          'texture': { type: 't', value: null }
        },
        vertexShader: [
          'varying vec3 vNormal;',
          'varying vec2 vUv;',
          'void main() {',
          'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
          'vNormal = normalize( normalMatrix * normal );',
          'vUv = uv;',
          '}'
        ].join('\n'),
        fragmentShader: [
          'uniform sampler2D texture;',
          'varying vec3 vNormal;',
          'varying vec2 vUv;',
          'void main() {',
          'vec3 diffuse = texture2D( texture, vUv ).xyz;',
          'float intensity = 1.05 - dot( vNormal, vec3( 0.0, 0.0, 1.0 ) );',
          'vec3 atmosphere = vec3( 1.0, 1.0, 1.0 ) * pow( intensity, 3.0 );',
          'gl_FragColor = vec4( diffuse + atmosphere, 1.0 );',
          '}'
        ].join('\n')
      },
      'atmosphere': {
        uniforms: {},
        vertexShader: [
          'varying vec3 vNormal;',
          'void main() {',
          'vNormal = normalize( normalMatrix * normal );',
          'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
          '}'
        ].join('\n'),
        fragmentShader: [
          'varying vec3 vNormal;',
          'void main() {',
          'float intensity = pow( 0.8 - dot( vNormal, vec3( 0, 0, 1.0 ) ), 12.0 );',
          'gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 ) * intensity;',
          '}'
        ].join('\n')
      }
    };
  }

  initGlobe() {
    this.options = this.options || {};
    this.imgDir = this.options.imgDir || 'assets/images/';
    this.isBigScreen = this.options.isBigScreen || false;

    this.container.style.color = '#fff';
    this.container.style.font = '13px/20px Arial, sans-serif';

    let shader, uniforms, material;
    this.w = this.container.offsetWidth || window.innerWidth;
    this.h = this.container.offsetHeight || window.innerHeight;
    this.camera = new THREE.PerspectiveCamera(30, (this.w + this.widthScaleFactor) / this.h, 1, 10000);
    this.camera.position.z = this.distance;

    this.scene = new THREE.Scene();

    const geometry = new THREE.SphereGeometry(200, 64, 64);

    shader = this.shaders['earth'];
    uniforms = THREE.UniformsUtils.clone(shader.uniforms);
    uniforms['texture'].value = new THREE.TextureLoader().load(this.imgDir + 'world.jpg');
    material = new THREE.ShaderMaterial({
      uniforms: uniforms,
      vertexShader: shader.vertexShader,
      fragmentShader: shader.fragmentShader
    });
    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.rotation.y = Math.PI;
    this.scene.add(this.mesh);

    this.webglRenderer = new THREE.WebGLRenderer({ antialias: true });
    this.webglRenderer.setSize(this.w, this.h);
    this.webglRenderer.domElement.style.position = 'absolute';
    this.container.appendChild(this.webglRenderer.domElement);

    // adding eventListener
    this.renderer.listen(this.container, 'mousedown', this.onMouseDown.bind(this));
    this.renderer.listen(this.container, 'mousewheel', this.onMouseWheel.bind(this));
    this.renderer.listen(this.container, 'click', this.onMouseAction.bind(this));
    this.renderer.listen(this.container, 'mousemove', this.onMouseAction.bind(this));
    this.renderer.listen(this.container, 'mouseover', (event) => {
      this.overRenderer = true;
      this.doRotate = false;
    });
    this.renderer.listen(this.container, 'mouseout', (event) => {
      this.overRenderer = false;
      this.doRotate = true;
    });
    this.renderer.listen(document, 'keydown', this.onDocumentKeyDown.bind(this));
    this.renderer.listen(window, 'resize', this.resizeWindow.bind(this));
  }

  getObjectOnEventFire(event, name?) {
    name = name || this.spikeName;
    event.preventDefault();
    const canvas = this.webglRenderer.domElement;
    const vector = new THREE.Vector3(((event.offsetX) / canvas.width) * 2 - 1, - ((event.offsetY) / canvas.height) * 2 + 1, 0.5);
    vector.unproject(this.camera);
    const ray = new THREE.Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());
    const intersects = ray.intersectObject(this.scene, true);
    if (intersects.length > 0) {
      const intersectObj = intersects[0];
      if (intersectObj.object.name !== name) {
        return null;
      }
      return intersectObj.object;
    }
    return null;
  }

  onMouseAction(event) {
    const spikeObj = this.getObjectOnEventFire(event, this.spikeName);
    const currentSelected = this.currentSelected || {};
    if (this.currentHovered && this.currentHovered.uuid !== currentSelected.uuid) {
      this.setSpikeColor(this.currentHovered);
      this.setSpikeDimension(this.currentHovered, false);
      this.currentHovered = null;
    }
    if (spikeObj) {
      this.container.style.cursor = 'pointer';
      this.currentHovered = spikeObj;
      spikeObj.material.color.setHex(this.hoverColor);
      this.setSpikeDimension(spikeObj, true);
    } else {
      this.container.style.cursor = 'default';
      if (event.type === 'click') {
        const popObj = this.getObjectOnEventFire(event, this.popName);
        if (popObj) {
          this.removePopover();
          this.setSpikeColor(this.currentSelected);
          this.setSpikeDimension(this.currentSelected, false);
          this.currentSelected = null;
        }
      }
      return;
    }
    if (event.type === 'click') {
      if (!_.isEmpty(this.currentSelected)) {
        this.setSpikeColor(this.currentSelected);
        this.setSpikeDimension(this.currentSelected, false);
      }
      this.currentSelected = spikeObj;
      this.addPop(spikeObj.position, spikeObj.userData);
      this.select.emit(spikeObj.userData);
    }
  }

  setSpikeColor(spikeObj, color?) {
    color = color || this.colorDef[spikeObj.userData[this.fieldMap.color]];
    spikeObj.material.color.setHex(color);
  }

  setSpikeDimension(spikeObj: any, isHovered?: boolean) {
    const dimension = isHovered ?  { x: 2.5, y: 2.5, z: spikeObj.scale.z } : { x: 1, y: 1, z: spikeObj.scale.z };
    spikeObj.scale.x = dimension.x;
    spikeObj.scale.y = dimension.y;
    spikeObj.scale.z = dimension.z;
  }

  // public refreshSpikes(ignoreSelected: boolean) {
  //   const allObjs = this.scene.children;
  //   allObjs.forEach(obj => {
  //     if (obj.name === this.spikeName) {
  //       if (!ignoreSelected || obj.uuid !== this.currentSelected.uuid) {
  //         const color = this.colorDef[obj.userData[this.fieldMap.color]];
  //         obj.material.color.setHex(color);
  //       }
  //     }
  //   });
  // }

  onMouseDown(event) {
    event.preventDefault();
    this.mouseMoveListener = this.renderer.listen(this.container, 'mousemove', this.onMouseMove.bind(this));
    this.mouseUpListener = this.renderer.listen(this.container, 'mouseup', this.onMouseUp.bind(this));
    this.mouseOutListener = this.renderer.listen(this.container, 'mouseout', this.onMouseOut.bind(this));

    this.mouseOnDown.x = - event.clientX;
    this.mouseOnDown.y = event.clientY;

    this.targetOnDown.x = this.target.x;
    this.targetOnDown.y = this.target.y;

    // this.container.style.cursor = 'move';
  }

  onMouseMove(event) {
    this.mouse.x = - event.clientX;
    this.mouse.y = event.clientY;

    const zoomDamp = this.distance / 1000;

    this.target.x = this.targetOnDown.x + (this.mouse.x - this.mouseOnDown.x) * 0.005 * zoomDamp;
    this.target.y = this.targetOnDown.y + (this.mouse.y - this.mouseOnDown.y) * 0.005 * zoomDamp;

    this.target.y = this.target.y > this.PI_HALF ? this.PI_HALF : this.target.y;
    this.target.y = this.target.y < - this.PI_HALF ? - this.PI_HALF : this.target.y;
  }

  onMouseUp(event) {
    // unsubscribe eventListeners
    this.mouseMoveListener();
    this.mouseUpListener();
    this.mouseOutListener();
    this.container.style.cursor = 'auto';
  }

  onMouseOut(event) {
    // unsubscribe eventListeners
    this.mouseMoveListener();
    this.mouseUpListener();
    this.mouseOutListener();
  }

  onMouseWheel(event) {
    event.preventDefault();
    if (this.overRenderer) {
      this.zoom(event.wheelDeltaY * 0.3);
    }
    return false;
  }

  onDocumentKeyDown(event) {
    switch (event.keyCode) {
      case 38:
        this.zoom(100);
        event.preventDefault();
        break;
      case 40:
        this.zoom(-100);
        event.preventDefault();
        break;
    }
  }

  zoom(delta) {
    this.distanceTarget -= delta;
    this.distanceTarget = this.distanceTarget > this.zoomFactor ? this.zoomFactor : this.distanceTarget;
  }

  addData(data) {
    this.removeAllPoints();
    this.options.setSpikeSize = this.options.setSpikeSize || function(val) { return val; };
    this.options.animated = this.options.animated || false;
    this.isRevolving = this.options.isRevolving || false;
    this.options.format = this.options.format || 'magnitude'; // other option is 'legend'
    this.isBigScreen = this.options.isBigScreen || false;
    this.isMobileScreen = this.options.isMobileScreen || false;
    const isSpikeThin = this.options.isSpikeThin || false;
    const position = this.options.setPosition || [];
    this.colorDef = this.options.colorDef || {};
    this.fieldMap = this.options.fieldMap || {
      latitude: 'latitude',
      longitude: 'longitude',
      value: 'value',
      color: 'color'
    };

    if (this.isBigScreen) {
      this.resizeContainer(this.w, this.h);
    }

    if (this.isMobileScreen) {
      this.zoomFactor = 1650;
    }

    if (position.length) {
      this.setTarget(position);
    }

    let geometry = new THREE.BoxGeometry(0.75, 0.75, 1);
    if (isSpikeThin) {
      geometry = new THREE.BoxGeometry(0.50, 0.50, 1);
    }

    data.forEach(dataPoint => {
      this.addSpike(geometry, dataPoint);
    });
    if (!this.isGlobeLoaded) {
      this.isGlobeLoaded = true;
      this.animate();
    }
  }

  removeAllPoints() {
    for (let i = this.scene.children.length - 1; i >= 0 ; i--) {
      const child = this.scene.children[ i ];
      if ( child.name === this.spikeName ) {
        this.scene.remove(child);
      }
    }
  }

  removePopover() {
    const popover = this.scene.getObjectByName(this.popoverName);
    this.scene.remove(popover);
  }

  addSpike(geometry, dataPoint) {
    const lat = dataPoint[this.fieldMap.latitude];
    const lng = dataPoint[this.fieldMap.longitude];
    const value = dataPoint[this.fieldMap.value];
    const size = this.options.setSpikeSize(value);

    const phi = (90 - lat) * Math.PI / 180;
    const theta = (180 - lng) * Math.PI / 180;

    const color = this.colorDef[dataPoint[this.fieldMap.color]];
    const spike = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({
      color: color
    }));

    spike.position.x = 200 * Math.sin(phi) * Math.cos(theta);
    spike.position.y = 200 * Math.cos(phi);
    spike.position.z = 200 * Math.sin(phi) * Math.sin(theta);

    spike.lookAt(this.mesh.position);
    // set the point height
    // if 1 then just a point
    // else spike
    spike.scale.z = Math.max(size, 0.1); // avoid non-invertible matrix
    spike.updateMatrix();
    spike.name = this.spikeName;
    spike.userData = dataPoint;
    this.scene.add(spike);
  }

  resizeWindow(event) {
    const w = this.container.getBoundingClientRect().width;
    const h = window.innerHeight;
    this.resizeContainer(w, h);
  }

  resizeContainer(w, h) {
    if (this.isBigScreen) {
      this.changeScaleFactor(w, h);
    }
    this.camera.aspect = (w + this.widthScaleFactor) / h;
    this.camera.updateProjectionMatrix();
    this.webglRenderer.setSize( w, h );
  }

  changeScaleFactor(w, h) {
    this.widthScaleFactor = this.maxWidthScaleValue;
  }

  animate() {
    requestAnimationFrame(this.animate.bind(this));
    if (this.isRevolving) {
      this.handleRotation();
    }
    this.handlePopScaling();
    this.render();
  }

  setRotation(doRotate: boolean) {
    this.isRevolving = doRotate;
  }

  handlePopScaling() {
    const scaleFactor = 8;
    const popover = this.scene.getObjectByName(this.popoverName);
    if (popover) {
      const popTxt = popover.children[0];
      const popBck = popover.children[1];
      const texts = popTxt.material.map.lines;
      const maxText = _.max(texts, t => t.length);
      const txtLen = maxText.length < 8 ? maxText.length * 22 : maxText.length < 15 ? maxText.length * 15 : maxText.length * 8;
      const scale = this.scaleVector.subVectors(popover.position, this.camera.position).length() / scaleFactor;
      popTxt.textSize = this.popoverTextSize / this.initialScale * scale;
      const w = txtLen / this.initialScale * scale;
      const h = this.popoverHeight / this.initialScale * scale;
      popBck.scale.set(w, h, 1);
      const center = this.currentSelected.position;
      const y = center.z < 0 ? center.y + (h / 1.55) : center.y + (h / 1.85);
      popover.position.set(center.x, y, center.z);
      // this.currentSelected.material.color.setHex(Math.random() * 0xffffff); // for blinking the spike
    }
  }

  handleRotation() {
    if (this.doRotate) {
      this.target.x -= 0.003;
    } else {
      this.target.x = this.target.x;
    }
  }

  setTarget(position: Array<number>) {
    this.target.x = position[0] || Math.PI * 3 / 2;
    this.target.y = position[1] || Math.PI / 6.0;
    this.distanceTarget = position[2] || 100000;
  }

  render() {
    this.zoom(this.curZoomSpeed);

    this.rotation.x += (this.target.x - this.rotation.x) * 0.3 - 100;
    this.rotation.y += (this.target.y - this.rotation.y) * 0.3 - 100;
    this.distance += (this.distanceTarget - this.distance) * 0.3;

    this.camera.position.x = this.distance * Math.sin(this.rotation.x) * Math.cos(this.rotation.y);
    this.camera.position.y = this.distance * Math.sin(this.rotation.y);
    this.camera.position.z = this.distance * Math.cos(this.rotation.x) * Math.cos(this.rotation.y);

    this.camera.lookAt(this.mesh.position);

    this.webglRenderer.render(this.scene, this.camera);
  }

  addPop(point, data) {
    this.removePopover();
    this.container.style.cursor = 'wait';
    this.geocoadingService.getReverseGeoInfo(data[this.fieldMap.latitude], data[this.fieldMap.longitude]).then((res) => {
      this.container.style.cursor = 'default';
      const place = res.address.village || res.address.city || res.address.state || res.address.country;
      const textData = [];
      textData.push('Location : ' + place);
      textData.push('Devices   : ' + data[this.fieldMap.value]);

      // adding text
      const text = new THREE.TextSprite({
        textSize: this.popoverTextSize,
        redrawInterval: 1,
        material: {
          color: 0xc3c3c3
        },
        texture: {
          text: textData.join('\n'),
          fontFamily: 'Honeywell Sans Web_Medium',
          textAlign: 'left',
          fontWeight: 'normal',
          autoRedraw: true
        }
      });
      text.name = this.popName;

      // adding background
      const canvas = document.createElement('canvas');
      const w = canvas.width = 300; // fixed here, dynamic in animate method
      const h = canvas.height = this.popoverHeight;
      const b = 10;
      const t = 10;
      const ctx = canvas.getContext('2d');
      ctx.fillStyle = 'rgba(75, 75, 75, .7)';
      ctx.beginPath();
      ctx.moveTo(0, t);
      ctx.quadraticCurveTo(0, t, 0, h - b);
      ctx.quadraticCurveTo(0, h - b, w / 2 - b, h - b);
      ctx.quadraticCurveTo(w / 2, h, w / 2, h);
      ctx.quadraticCurveTo(w / 2, h, w / 2 + b, h - b);
      ctx.quadraticCurveTo(w / 2, h - b, w, h - b);
      ctx.quadraticCurveTo(w, h - b, w, t);
      ctx.quadraticCurveTo(w, t, 0, t);
      ctx.fill();

      const backTexture = new THREE.Texture(canvas);
      backTexture.needsUpdate = true;
      const backMaterial = new THREE.SpriteMaterial( {
        map: backTexture
      });
      const background = new THREE.Sprite( backMaterial );
      background.name = this.popName;
      const popGroup = new THREE.Group();
      popGroup.add(text);
      popGroup.add(background);
      popGroup.name = this.popoverName;
      this.scene.add(popGroup);
      this.setPopTimeout();
    });
  }

  setPopTimeout() {
    clearTimeout(this.popTimeout);
    this.popTimeout = setTimeout(() => {
      this.removePopover();
      this.setSpikeColor(this.currentSelected);
      this.setSpikeDimension(this.currentSelected, false);
      this.currentSelected = null;
    }, 10000);
  }

  enableTouch() {
    const touchElement = new AlloyTouch({
        touch: this.webglRenderer.domElement,
        vertical: false,
        bindSelf : true,
        target: this.target,
        property: 'x',
        factor: 0.01,
        moveFactor: -0.01
    });
  }
}
