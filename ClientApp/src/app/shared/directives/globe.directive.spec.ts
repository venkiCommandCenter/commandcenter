import { inject, TestBed } from '@angular/core/testing';
import { ElementRef, Renderer2 } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GlobeDirective } from './globe.directive';
import { GeocoadingService } from '../services/geocoading.service';
import { LoggerService } from '../services/logger.service';

export class MockElementRef extends ElementRef {
  constructor() { super(null); }
}

describe('GlobeDirective', () => {
  let directive: GlobeDirective;
  const mockValue = { x: 0, y: 0 };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        MockElementRef,
        Renderer2,
        GeocoadingService,
        LoggerService
      ]
    });
  });

  beforeEach(inject([MockElementRef, Renderer2, GeocoadingService], (elementRef, renderer, geocoadingService) => {
    directive = new GlobeDirective(elementRef, renderer, geocoadingService);
  }));

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it(`should have defined 'container'`, () => {
    expect(directive.container).toBeDefined();
  });

  it(`should have defined 'widthScaleFactor' with '0'`, () => {
    expect(directive.widthScaleFactor).toBeDefined();
    expect(directive.widthScaleFactor).toEqual(0);
  });

  it(`should have defined 'maxWidthScaleValue' with '400'`, () => {
    expect(directive.maxWidthScaleValue).toBeDefined();
    expect(directive.maxWidthScaleValue).toEqual(400);
  });

  it(`should have defined 'curZoomSpeed' with '0'`, () => {
    expect(directive.curZoomSpeed).toBeDefined();
    expect(directive.curZoomSpeed).toEqual(0);
  });

  it(`should have defined 'zoomSpeed' with '50'`, () => {
    expect(directive.zoomSpeed).toBeDefined();
    expect(directive.zoomSpeed).toEqual(50);
  });

  it(`should have defined 'zoomFactor' with '1300'`, () => {
    expect(directive.zoomFactor).toBeDefined();
    expect(directive.zoomFactor).toEqual(1300);
  });

  it(`should have defined 'mouse' with ${JSON.stringify(mockValue)}`, () => {
    expect(directive.mouse).toBeDefined();
    expect(directive.mouse).toEqual(mockValue);
  });

  it(`should have defined 'mouseOnDown' with ${JSON.stringify(mockValue)}`, () => {
    expect(directive.mouseOnDown).toBeDefined();
    expect(directive.mouseOnDown).toEqual(mockValue);
  });

  it(`should have defined 'rotation' with ${JSON.stringify(mockValue)}`, () => {
    expect(directive.rotation).toBeDefined();
    expect(directive.rotation).toEqual(mockValue);
  });

  it(`should have defined 'targetOnDown' with ${JSON.stringify(mockValue)}`, () => {
    expect(directive.targetOnDown).toBeDefined();
    expect(directive.targetOnDown).toEqual(mockValue);
  });

  it(`should have defined 'target' with '{ x: Math.PI * 3 / 2, y: Math.PI / 6.0 }'`, () => {
    expect(directive.target).toBeDefined();
    expect(directive.target).toEqual({ x: Math.PI * 3 / 2, y: Math.PI / 6.0 });
  });

  it(`should have defined 'distanceTarget' with 100000`, () => {
    expect(directive.distanceTarget).toBeDefined();
    expect(directive.distanceTarget).toEqual(100000);
  });

  it(`should have defined 'distance' with 100000`, () => {
    expect(directive.distance).toBeDefined();
    expect(directive.distance).toEqual(100000);
  });

  it(`should have defined 'PI_HALF' with 'Math.PI / 2'`, () => {
    expect(directive.PI_HALF).toBeDefined();
    expect(directive.PI_HALF).toEqual(Math.PI / 2);
  });

  it(`should have defined 'doRotate' with true`, () => {
    expect(directive.doRotate).toBeDefined();
    expect(directive.doRotate).toBe(true);
  });

  it(`should have defined 'isRevolving' with true`, () => {
    expect(directive.isRevolving).toBeDefined();
    expect(directive.isRevolving).toBe(false);
  });

  it(`should have defined 'hoverColor' with '0xffffff'`, () => {
    expect(directive.hoverColor).toBeDefined();
    expect(directive.hoverColor).toEqual(0xffffff);
  });

  it(`should have defined 'isHovered' with false`, () => {
    expect(directive.isHovered).toBeDefined();
    expect(directive.isHovered).toBe(false);
  });

  it(`should have defined 'spikeName' with 'spike'`, () => {
    expect(directive.spikeName).toBeDefined();
    expect(directive.spikeName).toEqual('spike');
  });

  it(`should have defined 'popName' with 'pop-element'`, () => {
    expect(directive.popName).toBeDefined();
    expect(directive.popName).toEqual('pop-element');
  });

  it(`should have defined 'popoverName' with 'popover'`, () => {
    expect(directive.popoverName).toBeDefined();
    expect(directive.popoverName).toEqual('popover');
  });

  it(`should have defined 'popoverHeight' with '70'`, () => {
    expect(directive.popoverHeight).toBeDefined();
    expect(directive.popoverHeight).toEqual(70);
  });

  it(`should have defined 'popoverTextSize' with '15'`, () => {
    expect(directive.popoverTextSize).toBeDefined();
    expect(directive.popoverTextSize).toEqual(15);
  });

  it(`should have defined 'initialScale' with '138'`, () => {
    expect(directive.initialScale).toBeDefined();
    expect(directive.initialScale).toEqual(138);
  });

  it(`should have defined 'isGlobeLoaded' with 'false'`, () => {
    expect(directive.isGlobeLoaded).toBeDefined();
    expect(directive.isGlobeLoaded).toEqual(false);
  });

  it(`should have called 'setShader()' with no arguments`, () => {
    spyOn(directive, 'setShader');
    directive.setShader();
    expect(directive.setShader).toBeDefined();
    expect(directive.setShader).toHaveBeenCalled();
    expect(directive.setShader).toHaveBeenCalledWith();
  });

  it(`should have called 'initGlobe()' with no arguments`, () => {
    spyOn(directive, 'initGlobe');
    directive.initGlobe();
    expect(directive.initGlobe).toBeDefined();
    expect(directive.initGlobe).toHaveBeenCalled();
    expect(directive.initGlobe).toHaveBeenCalledWith();
  });

  it(`should have called 'getObjectOnEventFire()' with all possible arguments`, () => {
    spyOn(directive, 'getObjectOnEventFire');
    directive.getObjectOnEventFire({});
    directive.getObjectOnEventFire({}, 'spike');
    directive.getObjectOnEventFire(null);
    directive.getObjectOnEventFire(null, null);
    expect(directive.getObjectOnEventFire).toBeDefined();
    expect(directive.getObjectOnEventFire).toHaveBeenCalled();
    expect(directive.getObjectOnEventFire).toHaveBeenCalledWith({});
    expect(directive.getObjectOnEventFire).toHaveBeenCalledWith({}, 'spike');
    expect(directive.getObjectOnEventFire).toHaveBeenCalledWith(null);
    expect(directive.getObjectOnEventFire).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'onMouseAction()' with all possible arguments`, () => {
    spyOn(directive, 'onMouseAction');
    directive.onMouseAction({});
    directive.onMouseAction(null);
    expect(directive.onMouseAction).toBeDefined();
    expect(directive.onMouseAction).toHaveBeenCalled();
    expect(directive.onMouseAction).toHaveBeenCalledWith({});
    expect(directive.onMouseAction).toHaveBeenCalledWith(null);
  });

  it(`should have called 'setSpikeColor()' with all possible arguments`, () => {
    spyOn(directive, 'setSpikeColor');
    directive.setSpikeColor({});
    directive.setSpikeColor({}, 'red');
    directive.setSpikeColor(null);
    directive.setSpikeColor(null, null);
    expect(directive.setSpikeColor).toBeDefined();
    expect(directive.setSpikeColor).toHaveBeenCalled();
    expect(directive.setSpikeColor).toHaveBeenCalledWith({});
    expect(directive.setSpikeColor).toHaveBeenCalledWith({}, 'red');
    expect(directive.setSpikeColor).toHaveBeenCalledWith(null);
    expect(directive.setSpikeColor).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'setSpikeDimension()' with all possible arguments`, () => {
    spyOn(directive, 'setSpikeDimension');
    directive.setSpikeDimension({});
    directive.setSpikeDimension({}, true);
    directive.setSpikeDimension({}, false);
    directive.setSpikeDimension(null);
    directive.setSpikeDimension(null, null);
    expect(directive.setSpikeDimension).toBeDefined();
    expect(directive.setSpikeDimension).toHaveBeenCalled();
    expect(directive.setSpikeDimension).toHaveBeenCalledWith({});
    expect(directive.setSpikeDimension).toHaveBeenCalledWith({}, true);
    expect(directive.setSpikeDimension).toHaveBeenCalledWith({}, false);
    expect(directive.setSpikeDimension).toHaveBeenCalledWith(null);
    expect(directive.setSpikeDimension).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'onMouseDown()' with all possible arguments`, () => {
    spyOn(directive, 'onMouseDown');
    directive.onMouseDown({});
    directive.onMouseDown(null);
    expect(directive.onMouseDown).toBeDefined();
    expect(directive.onMouseDown).toHaveBeenCalled();
    expect(directive.onMouseDown).toHaveBeenCalledWith({});
    expect(directive.onMouseDown).toHaveBeenCalledWith(null);
  });

  it(`should have called 'onMouseMove()' with all possible arguments`, () => {
    spyOn(directive, 'onMouseMove');
    directive.onMouseMove({});
    directive.onMouseMove(null);
    expect(directive.onMouseMove).toBeDefined();
    expect(directive.onMouseMove).toHaveBeenCalled();
    expect(directive.onMouseMove).toHaveBeenCalledWith({});
    expect(directive.onMouseMove).toHaveBeenCalledWith(null);
  });

  it(`should have called 'onMouseUp()' with all possible arguments`, () => {
    spyOn(directive, 'onMouseUp');
    directive.onMouseUp({});
    directive.onMouseUp(null);
    expect(directive.onMouseUp).toBeDefined();
    expect(directive.onMouseUp).toHaveBeenCalled();
    expect(directive.onMouseUp).toHaveBeenCalledWith({});
    expect(directive.onMouseUp).toHaveBeenCalledWith(null);
  });

  it(`should have called 'onMouseOut()' with all possible arguments`, () => {
    spyOn(directive, 'onMouseOut');
    directive.onMouseOut({});
    directive.onMouseOut(null);
    expect(directive.onMouseOut).toBeDefined();
    expect(directive.onMouseOut).toHaveBeenCalled();
    expect(directive.onMouseOut).toHaveBeenCalledWith({});
    expect(directive.onMouseOut).toHaveBeenCalledWith(null);
  });

  it(`should have called 'onMouseWheel()' with all possible arguments`, () => {
    spyOn(directive, 'onMouseWheel');
    directive.onMouseWheel({});
    directive.onMouseWheel(null);
    expect(directive.onMouseWheel).toBeDefined();
    expect(directive.onMouseWheel).toHaveBeenCalled();
    expect(directive.onMouseWheel).toHaveBeenCalledWith({});
    expect(directive.onMouseWheel).toHaveBeenCalledWith(null);
  });

  it(`should have called 'onDocumentKeyDown()' with all possible arguments`, () => {
    spyOn(directive, 'onDocumentKeyDown');
    directive.onDocumentKeyDown({});
    directive.onDocumentKeyDown(null);
    expect(directive.onDocumentKeyDown).toBeDefined();
    expect(directive.onDocumentKeyDown).toHaveBeenCalled();
    expect(directive.onDocumentKeyDown).toHaveBeenCalledWith({});
    expect(directive.onDocumentKeyDown).toHaveBeenCalledWith(null);
  });

  it(`should have called 'zoom()' with all possible arguments`, () => {
    spyOn(directive, 'zoom');
    directive.zoom({});
    directive.zoom(null);
    expect(directive.zoom).toBeDefined();
    expect(directive.zoom).toHaveBeenCalled();
    expect(directive.zoom).toHaveBeenCalledWith({});
    expect(directive.zoom).toHaveBeenCalledWith(null);
  });

  it(`should have called 'addData()' with all possible arguments`, () => {
    spyOn(directive, 'addData');
    directive.addData({});
    directive.addData(null);
    expect(directive.addData).toBeDefined();
    expect(directive.addData).toHaveBeenCalled();
    expect(directive.addData).toHaveBeenCalledWith({});
    expect(directive.addData).toHaveBeenCalledWith(null);
  });

  it(`should have called 'removeAllPoints()' with no arguments`, () => {
    spyOn(directive, 'removeAllPoints');
    directive.removeAllPoints();
    expect(directive.removeAllPoints).toBeDefined();
    expect(directive.removeAllPoints).toHaveBeenCalled();
    expect(directive.removeAllPoints).toHaveBeenCalledWith();
  });

  it(`should have called 'removePopover()' with no arguments`, () => {
    spyOn(directive, 'removePopover');
    directive.removePopover();
    expect(directive.removePopover).toBeDefined();
    expect(directive.removePopover).toHaveBeenCalled();
    expect(directive.removePopover).toHaveBeenCalledWith();
  });

  it(`should have called 'addSpike()' with all possible arguments`, () => {
    spyOn(directive, 'addSpike');
    directive.addSpike({}, {});
    directive.addSpike({}, null);
    directive.addSpike(null, {});
    directive.addSpike(null, null);
    expect(directive.addSpike).toBeDefined();
    expect(directive.addSpike).toHaveBeenCalled();
    expect(directive.addSpike).toHaveBeenCalledWith({}, null);
    expect(directive.addSpike).toHaveBeenCalledWith({}, {});
    expect(directive.addSpike).toHaveBeenCalledWith(null, {});
    expect(directive.addSpike).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'resizeWindow()' with all possible arguments`, () => {
    spyOn(directive, 'resizeWindow');
    directive.resizeWindow({});
    directive.resizeWindow(null);
    expect(directive.resizeWindow).toBeDefined();
    expect(directive.resizeWindow).toHaveBeenCalled();
    expect(directive.resizeWindow).toHaveBeenCalledWith({});
    expect(directive.resizeWindow).toHaveBeenCalledWith(null);
  });

  it(`should have called 'resizeContainer()' with all possible arguments`, () => {
    spyOn(directive, 'resizeContainer');
    directive.resizeContainer(100, 50);
    directive.resizeContainer(100, null);
    directive.resizeContainer(null, 50);
    directive.resizeContainer(null, null);
    expect(directive.resizeContainer).toBeDefined();
    expect(directive.resizeContainer).toHaveBeenCalled();
    expect(directive.resizeContainer).toHaveBeenCalledWith(100, 50);
    expect(directive.resizeContainer).toHaveBeenCalledWith(100, null);
    expect(directive.resizeContainer).toHaveBeenCalledWith(null, 50);
    expect(directive.resizeContainer).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'changeScaleFactor()' with all possible arguments`, () => {
    spyOn(directive, 'changeScaleFactor');
    directive.changeScaleFactor(100, 50);
    directive.changeScaleFactor(100, null);
    directive.changeScaleFactor(null, 50);
    directive.changeScaleFactor(null, null);
    expect(directive.changeScaleFactor).toBeDefined();
    expect(directive.changeScaleFactor).toHaveBeenCalled();
    expect(directive.changeScaleFactor).toHaveBeenCalledWith(100, 50);
    expect(directive.changeScaleFactor).toHaveBeenCalledWith(100, null);
    expect(directive.changeScaleFactor).toHaveBeenCalledWith(null, 50);
    expect(directive.changeScaleFactor).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'animate()' with no arguments`, () => {
    spyOn(directive, 'animate');
    directive.animate();
    expect(directive.animate).toBeDefined();
    expect(directive.animate).toHaveBeenCalled();
    expect(directive.animate).toHaveBeenCalledWith();
  });

  it(`should have called 'setRotation()' with all possible arguments`, () => {
    spyOn(directive, 'setRotation');
    directive.setRotation(true);
    directive.setRotation(false);
    directive.setRotation(null);
    expect(directive.setRotation).toBeDefined();
    expect(directive.setRotation).toHaveBeenCalled();
    expect(directive.setRotation).toHaveBeenCalledWith(true);
    expect(directive.setRotation).toHaveBeenCalledWith(false);
    expect(directive.setRotation).toHaveBeenCalledWith(null);
  });

  it(`should have called 'handlePopScaling()' with no arguments`, () => {
    spyOn(directive, 'handlePopScaling');
    directive.handlePopScaling();
    expect(directive.handlePopScaling).toBeDefined();
    expect(directive.handlePopScaling).toHaveBeenCalled();
    expect(directive.handlePopScaling).toHaveBeenCalledWith();
  });

  it(`should have called 'handleRotation()' with no arguments`, () => {
    spyOn(directive, 'handleRotation');
    directive.handleRotation();
    expect(directive.handleRotation).toBeDefined();
    expect(directive.handleRotation).toHaveBeenCalled();
    expect(directive.handleRotation).toHaveBeenCalledWith();
  });

  it(`should have called 'setTarget()' with all possible arguments`, () => {
    spyOn(directive, 'setTarget');
    directive.setTarget([]);
    directive.setTarget([100]);
    directive.setTarget([100, 200]);
    directive.setTarget(null);
    expect(directive.setTarget).toBeDefined();
    expect(directive.setTarget).toHaveBeenCalled();
    expect(directive.setTarget).toHaveBeenCalledWith([]);
    expect(directive.setTarget).toHaveBeenCalledWith([100]);
    expect(directive.setTarget).toHaveBeenCalledWith([100, 200]);
    expect(directive.setTarget).toHaveBeenCalledWith(null);
  });

  it(`should have called 'render()' with no arguments`, () => {
    spyOn(directive, 'render');
    directive.render();
    expect(directive.render).toBeDefined();
    expect(directive.render).toHaveBeenCalled();
    expect(directive.render).toHaveBeenCalledWith();
  });

  it(`should have called 'addPop()' with all possible arguments`, () => {
    spyOn(directive, 'addPop');
    directive.addPop({}, {});
    directive.addPop({}, null);
    directive.addPop(null, {});
    directive.addPop(null, null);
    expect(directive.addPop).toBeDefined();
    expect(directive.addPop).toHaveBeenCalled();
    expect(directive.addPop).toHaveBeenCalledWith({}, null);
    expect(directive.addPop).toHaveBeenCalledWith({}, {});
    expect(directive.addPop).toHaveBeenCalledWith(null, {});
    expect(directive.addPop).toHaveBeenCalledWith(null, null);
  });

  it(`should have called 'setPopTimeout()' with no arguments`, () => {
    spyOn(directive, 'setPopTimeout');
    directive.setPopTimeout();
    expect(directive.setPopTimeout).toBeDefined();
    expect(directive.setPopTimeout).toHaveBeenCalled();
    expect(directive.setPopTimeout).toHaveBeenCalledWith();
  });

  it(`should have called 'enableTouch()' with no arguments`, () => {
    spyOn(directive, 'enableTouch');
    directive.enableTouch();
    expect(directive.enableTouch).toBeDefined();
    expect(directive.enableTouch).toHaveBeenCalled();
    expect(directive.enableTouch).toHaveBeenCalledWith();
  });
});
