import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { GeocoadingService } from './geocoading.service';
import { LoggerService } from './logger.service';
import { Constants } from '../constants';

describe('GeocoadingService', () => {
  let service: GeocoadingService;
  let http: HttpClient;
  let httpController: HttpTestingController;
  let testUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        GeocoadingService,
        LoggerService
      ]
    });

    service = TestBed.get(GeocoadingService);
    http = TestBed.get(HttpClient);
    httpController = TestBed.get(HttpTestingController);
    testUrl = `${Constants.geoBaseURL}format=json&lat=${32.7}&lon=${88.2}&zoom=18&addressdetails=1`;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`'getReverseGeoInfo: GET' should return correct response`, () => {
    const dummyResponse = {
      'place_id': '236693682',
      'licence': 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
      'osm_type': 'way',
      'osm_id': '404407281',
      'lat': '32.71058',
      'lon': '88.157507',
      'display_name': '双陈线, Shuanghu County, Nagqu City, Tibet Autonomous Region, PRC',
      'address': {
        'road': '双陈线',
        'county': 'Shuanghu County',
        'region': 'Nagqu City',
        'state': 'Tibet Autonomous Region',
        'country': 'PRC',
        'country_code': 'cn'
      },
      'boundingbox': [
        '32.607862',
        '32.788891',
        '88.110626',
        '88.241055'
      ]
    };

    http.get(testUrl)
      .subscribe(res => {
        expect(res).toEqual(dummyResponse);
      });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });
});
