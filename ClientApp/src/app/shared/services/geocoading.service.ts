import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../constants';
import { LoggerService } from './logger.service';

@Injectable()
export class GeocoadingService {

  constructor(
    private http: HttpClient,
    private loggerService: LoggerService
  ) { }

  getReverseGeoInfo(lat, long): Promise<any> {
    const format = 'json';
    const url = `${Constants.geoBaseURL}format=${format}&lat=${lat}&lon=${long}&zoom=18&addressdetails=1`;
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        this.loggerService.info(`${url} is called successfully`);
        resolve(res);
      }, err => {
        this.loggerService.error(`${url} failed!`, err);
        reject(err);
      });
    });
  }

}
