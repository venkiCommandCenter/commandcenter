import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../constants';

@Injectable()
export class LoggerService {

  url = Constants.baseURL + 'api/UILogging';

  constructor(
    private http: HttpClient
  ) { }

  info(msg: string, info?: any) {
    const body = {
      Message: 'Information',
      Exception: msg + (info ? ' ' + JSON.stringify(info) : ''),
      Parameters: '1'
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    this.http.post(this.url, body, httpOptions).subscribe(res => {
      console.log(body.Exception, 'Logged on server successfully');
    }, err => {
      console.log(body.Exception, 'Logged on server failed!');
    });
  }

  error(msg: string, error?: any) {
    const body = {
      Message: msg,
      Exception: error ? JSON.stringify(error) : null,
      Parameters: 0
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    this.http.post(this.url, body, httpOptions).subscribe(res => {
      console.log(body.Exception + ' => ' + body.Exception, 'Logged on server successfully');
    }, err => {
      console.log(body.Exception + ' => ' + body.Exception, 'Logged on server failed!');
    });
  }
}
