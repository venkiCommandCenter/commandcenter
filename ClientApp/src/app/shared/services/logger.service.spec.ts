import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoggerService } from './logger.service';
import { Constants } from '../constants';

describe('LoggerService', () => {
  let service: LoggerService;
  let http: HttpClient;
  let httpController: HttpTestingController;
  let testUrl: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [LoggerService]
    });

    service = TestBed.get(LoggerService);
    http = TestBed.get(HttpClient);
    httpController = TestBed.get(HttpTestingController);
    testUrl = `${Constants.baseURL}api/UILogging`;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`'info: POST' should return correct response`, () => {
    const dummyRequest = {
      Message: 'Information',
      Exception: 'Dummy exception',
      Parameters: '1'
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    http.post(testUrl, dummyRequest, httpOptions)
      .subscribe(res => {
        expect(res).toEqual('success');
      });

    const req = httpController.expectOne(testUrl);
    expect(req.request.headers.has('Content-Type'));
    expect(req.request.method).toEqual('POST');

    req.flush('success');
    httpController.verify();
  });

  it(`'error: POST' should return correct response`, () => {
    const dummyRequest = {
      Message: 'Error',
      Exception: 'Dummy exception',
      Parameters: '1'
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    http.post(testUrl, dummyRequest, httpOptions)
      .subscribe(res => {
        expect(res).toEqual('success');
      });

    const req = httpController.expectOne(testUrl);
    expect(req.request.headers.has('Content-Type'));
    expect(req.request.method).toEqual('POST');

    req.flush('success');
    httpController.verify();
  });
});
