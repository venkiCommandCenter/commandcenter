import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DeviceDetectorModule, DeviceDetectorService } from 'ngx-device-detector';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        RouterTestingModule,
        DeviceDetectorModule
      ],
      providers: [
        DeviceDetectorService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('openModal => function should be defined', () => {
    expect(component.openModal).toBeDefined();
  });

  it('closeModal => function should be defined', () => {
    expect(component.closeModal).toBeDefined();
  });

  it('isBrowserZoomed => function should be defined', () => {
    expect(component.isBrowserZoomed).toBeDefined();
  });

  it('isBrowserZoomed => function should return boolean', () => {
    expect(typeof(component.isBrowserZoomed())).toEqual('boolean');
  });

  it('should have a router-outlet tag', () => {
    expect(compiled.querySelector('router-outlet')).not.toBeNull();
  });

  it('should have a modal popup', () => {
    expect(compiled.querySelector('#popup')).not.toBeNull();
    expect(compiled.querySelector('#popup').attributes.getNamedItem('class').value).toContain('modal');
  });
});
