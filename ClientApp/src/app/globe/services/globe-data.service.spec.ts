import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { GlobeDataService } from './globe-data.service';
import { LoggerService } from '../../shared/services/logger.service';
import { Constants } from '../../shared/constants';

describe('GlobeDataService', () => {
  let service: GlobeDataService;
  let http: HttpClient;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        GlobeDataService,
        LoggerService
      ]
    });
    service = TestBed.get(GlobeDataService);
    http = TestBed.get(HttpClient);
    httpController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`'getConnectedDevices: GET' should return correct response`, () => {
    const testUrl = Constants.baseURL + 'api/ConnectedDevicePlot';
    const dummyResponse = [
      {
        'id': 0,
        'lattitude': '-26.23090',
        'longitude': '28.05830',
        'deviceCount': '8',
        'intensity': 'red',
        'geoLocation': null,
        'sbg': null
      },
      {
        'id': 1,
        'lattitude': '-27.41550',
        'longitude': '152.97660',
        'deviceCount': '2',
        'intensity': 'blue',
        'geoLocation': null,
        'sbg': null
      }
    ];

    http.get(testUrl).subscribe(res => {
      expect(res).toEqual(dummyResponse);
    });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });

  it(`'getConnectedDeviceSummary: GET' should return correct response`, () => {
    const testUrl = Constants.baseURL + 'api/getConnectedDeviceSummary/0/ALL';
    const dummyResponse = [
      {
        'id': 99,
        'connectedDeviceLabel': 'Total Gateways',
        'value': '2500',
        'units': null,
        'sbg': 'ALL',
        'previous90Days': '2550',
        'geoLocationId': 0
      },
      {
        'id': 100,
        'connectedDeviceLabel': 'Gateway Provisioned / 90 days',
        'value': '1975',
        'units': null,
        'sbg': 'ALL',
        'previous90Days': '1300',
        'geoLocationId': 0
      }
   ];

    http.get(testUrl).subscribe(res => {
      expect(res).toEqual(dummyResponse);
    });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });

  it(`'getNewsFeedData: GET' should return correct response`, () => {
    const testUrl = Constants.baseURL + 'api/NewsFeedAlert';
    const dummyResponse = {
      'newsId': 15,
      'currentNewsFeed': 'Hurricane in Barbados',
      'nextNewsFeed': 'Server Events',
      'tomorrowNewsFeed': 'Weekend'
    };

    http.get(testUrl).subscribe(res => {
      expect(res).toEqual(dummyResponse);
    });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });
});
