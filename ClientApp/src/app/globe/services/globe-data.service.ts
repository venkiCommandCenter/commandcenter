import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../shared/constants';
import { LoggerService } from '../../shared/services/logger.service';

@Injectable()
export class GlobeDataService {
  constructor(
    private http: HttpClient,
    private loggerService: LoggerService
  ) { }

  getConnectedDevices(bu?: string): Promise<any> {
    bu = bu && bu !== 'ALL' ? '/' + bu : '';
    const url = Constants.baseURL + 'api/ConnectedDevicePlot' + bu;
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        this.loggerService.info(`${url} is called successfully`);
        resolve(res);
      }, err => {
        this.loggerService.error(`${url} failed!`, err);
        reject(err);
      });
    });
  }

  getConnectedDeviceSummary(bu?: string, reg?: string): Promise<any> {
    reg = reg ? '/' + reg : '/0';
    bu = bu ? '/' + bu : '';
    const url = Constants.baseURL + 'api/ConnectedDeviceSummary' + reg + bu;
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        this.loggerService.info(`${url} is called successfully`);
        resolve(res);
      }, err => {
        this.loggerService.error(`${url} failed!`, err);
        reject(err);
      });
    });
  }

  // getOutagesWorldDevices(): Promise<any> {
  //   const url = Constants.baseURL + 'api/OutagesDevicePlot/0';
  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(res => {
  //       this.loggerService.info(`${url} is called successfully`);
  //       resolve(res);
  //     }, err => {
  //       this.loggerService.error(`${url} failed!`, err);
  //       reject(err);
  //     });
  //   });
  // }

  // getOutagesWorldSummery(): Promise<any> {
  //   const url = Constants.baseURL + 'api/OutagesDeviceSummary/0';
  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(res => {
  //       this.loggerService.info(`${url} is called successfully`);
  //       resolve(res);
  //     }, err => {
  //       this.loggerService.error(`${url} failed!`, err);
  //       reject(err);
  //     });
  //   });
  // }

  // getOutagesUsDevices(bu?: string): Promise<any> {
  //   bu = bu ? '/' + bu : '';
  //   const url = Constants.baseURL + 'api/NorthAmericaDevicePlot/1' + bu;
  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(res => {
  //       this.loggerService.info(`${url} is called successfully`);
  //       resolve(res);
  //     }, err => {
  //       this.loggerService.error(`${url} failed!`, err);
  //       reject(err);
  //     });
  //   });
  // }

  // getRegionalConnectedDevices(bu?: string, reg?: number): Promise<any> {
  //   bu = bu ? '/' + bu : '';
  //   reg = reg ? reg : 1;
  //   const url = Constants.baseURL + `api/NorthAmericaDevicePlot/${reg}${bu}`;
  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(res => {
  //       this.loggerService.info(`${url} is called successfully`);
  //       resolve(res);
  //     }, err => {
  //       this.loggerService.error(`${url} failed!`, err);
  //       reject(err);
  //     });
  //   });
  // }

  // getOutagesUsSummery(bu?: string): Promise<any> {
  //   bu = bu ? '/' + bu : '';
  //   const url = Constants.baseURL + 'api/NorthAmericaDeviceSummary/1' + bu;
  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(res => {
  //       this.loggerService.info(`${url} is called successfully`);
  //       resolve(res);
  //     }, err => {
  //       this.loggerService.error(`${url} failed!`, err);
  //       reject(err);
  //     });
  //   });
  // }

  getNewsFeedData(): Promise<any> {
    const url = Constants.baseURL + 'api/NewsFeedAlert';
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        this.loggerService.info(`${url} is called successfully`);
        resolve(res);
      }, err => {
        this.loggerService.error(`${url} failed!`, err);
        reject(err);
      });
    });
  }

  // getBlobImages(): Promise<any> {
  //   const url = Constants.baseURL + 'api/BlobImages';
  //   return new Promise((resolve, reject) => {
  //     this.http.get(url).subscribe(res => {
  //       this.loggerService.info(`${url} is called successfully`);
  //       resolve(res);
  //     }, err => {
  //       this.loggerService.error(`${url} failed!`, err);
  //       reject(err);
  //     });
  //   });
  // }

  downloadReport(bu?: string, reg?: string): void {
    bu = bu ? '/' + bu : '';
    reg = reg ? '/' + reg : '/' + 0;
    window.location.assign(Constants.baseURL + `api/DownloadKPIData${reg}${bu}`);
  }
}

