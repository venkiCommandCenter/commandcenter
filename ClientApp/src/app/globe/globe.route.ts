import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConnectedDevicesComponent } from './components/connected-devices/connected-devices.component';
import { GlobeCarouselComponent } from './components/globe-carousel/globe-carousel.component';

const globeRoutes: Routes = [
    {
        path: '',
        component: GlobeCarouselComponent
    },
    {
        path: 'connected-devices',
        component: ConnectedDevicesComponent
    },
    {
        path: 'big',
        component: GlobeCarouselComponent,
        data: {
            bigScreen: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(globeRoutes)
    ], exports: [
        RouterModule
    ]
})

export class GlobeRouteModule {}
