import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConnectedDevicesComponent } from '../connected-devices/connected-devices.component';
import { GlobeCarouselComponent } from './globe-carousel.component';
import { SharedModule } from '../../../shared/shared.module';
import { GlobeDataService } from '../../services/globe-data.service';

describe('GlobeCarouselComponent', () => {
  let component: GlobeCarouselComponent;
  let fixture: ComponentFixture<GlobeCarouselComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GlobeCarouselComponent,
        ConnectedDevicesComponent
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule
      ],
      providers: [
        GlobeDataService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobeCarouselComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have the tag 'app-connected-devices'`, () => {
    expect(compiled.querySelector('app-connected-devices')).not.toBeNull();
  });
});
