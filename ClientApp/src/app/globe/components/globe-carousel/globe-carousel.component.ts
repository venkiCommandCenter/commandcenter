import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-globe-carousel',
  templateUrl: './globe-carousel.component.html',
  styleUrls: ['./globe-carousel.component.css']
})
export class GlobeCarouselComponent implements OnInit {

  activeSlide = 0;
  isBigScreen = false;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.isBigScreen = this.route.snapshot.data['bigScreen'];
  }

  enableSlide(idx) {
    this.activeSlide = idx;
  }

}
