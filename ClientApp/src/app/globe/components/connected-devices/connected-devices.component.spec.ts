import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConnectedDevicesComponent } from './connected-devices.component';
import { SharedModule } from '../../../shared/shared.module';
import { GlobeDataService } from '../../services/globe-data.service';

describe('ConnectedDevicesComponent', () => {
  let component: ConnectedDevicesComponent;
  let fixture: ComponentFixture<ConnectedDevicesComponent>;
  let compiled: any;
  const colorDefStub: any = { red: 0xFF0000, green: 0x00FF7F, blue: 0x00FF7F };
  const businessUnitsStub: string[] = ['ALL', 'HBT', 'PMT', 'SPS'];
  const regions: any[] = [
    {
      name: 'GLOBAL',
      geoLocation: 0
    }, {
      name: 'NORTH AMERICA',
      geoLocation: 1
    }, {
      name: 'APAC',
      geoLocation: 2
    }, {
      name: 'EMEA',
      geoLocation: 3
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ConnectedDevicesComponent
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule
      ],
      providers: [
        GlobeDataService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedDevicesComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have the title 'Sentience Operations Center'`, () => {
    expect(compiled.querySelector('.subHeader').textContent).toContain('Sentience Operations Center');
  });

  it(`logo image should be 'Honeywell-logo-2015_RGB_Red-sm.png'`, () => {
    expect(compiled.querySelector('.logo-img').getAttribute('src')).toContain('Honeywell-logo-2015_RGB_Red-sm.png');
  });

  it(`should have assigned 'colorDef' with ${JSON.stringify(colorDefStub)}`, () => {
    expect(component.colorDef).toBeDefined();
    expect(component.colorDef).toEqual(colorDefStub);
  });

  it(`should have assigned 'businessUnits' with ${JSON.stringify(businessUnitsStub)}`, () => {
    expect(component.businessUnits).toBeDefined();
    expect(component.businessUnits).toEqual(businessUnitsStub);
  });

  describe(`'regions' field`, () => {
    regions.forEach((reg) => {
      it(`should contains 'geoLocation' ${reg.geoLocation} for ${reg.name}`, () => {
        expect(component.regions).toContain(jasmine.objectContaining(reg));
      });
    });
  });

  it(`should have assigned 'selectedBu' with ${JSON.stringify(businessUnitsStub[0])}`, () => {
    expect(component.selectedBu).toBeDefined();
    expect((component.selectedBu === businessUnitsStub[0]) || (component.selectedBu === null)).toBe(true);
  });

  it(`should have assigned 'selectedRegion' with ${JSON.stringify(regions[0])}`, () => {
    expect(component.selectedRegion).toBeDefined();
    expect(component.selectedRegion).toEqual(jasmine.objectContaining(regions[0]));
  });

  it(`should have defined 'globe'`, () => {
    expect(component.globe).toBeDefined();
  });

  it(`should have called 'drawGlobe()' with no arguments`, () => {
    spyOn(component, 'drawGlobe');
    component.drawGlobe();
    expect(component.drawGlobe).toBeDefined();
    expect(component.drawGlobe).toHaveBeenCalled();
    expect(component.drawGlobe).toHaveBeenCalledWith();
  });

  it(`should have called 'getDeviceSummary()' with no arguments`, () => {
    spyOn(component, 'getDeviceSummary');
    component.getDeviceSummary();
    expect(component.getDeviceSummary).toBeDefined();
    expect(component.getDeviceSummary).toHaveBeenCalled();
    expect(component.getDeviceSummary).toHaveBeenCalledWith();
  });

  it(`'checkDecimals' function should return correct output`, () => {
    expect(component.checkDecimals).toBeDefined();
    expect(component.checkDecimals('5')).toBeFalsy();
    expect(component.checkDecimals('1.5')).toBeTruthy();
    expect(component.checkDecimals(null)).toBeFalsy();
  });

  it(`'trimUnit' function should return correct output`, () => {
    expect(component.trimUnit).toBeDefined();
    expect(component.trimUnit('K')).toEqual('K');
    expect(component.trimUnit('GB')).toEqual('GB');
    expect(component.trimUnit('Mile')).toEqual('Mi');
    expect(component.trimUnit(null)).toBeNull();
  });

  it(`'formatKpiLabel' function should return correct output`, () => {
    expect(component.formatKpiLabel).toBeDefined();
    expect(component.formatKpiLabel('Gateway Provisioned/90 days')).toEqual(['Gateway Provisioned', '90 days']);
    expect(component.formatKpiLabel('Total Gateways')).toEqual(['Total Gateways']);
    expect(component.formatKpiLabel('Global Message Traffic / 30 days')).toEqual(['Global Message Traffic', '30 days']);
  });
});
