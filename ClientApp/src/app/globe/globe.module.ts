import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

// routes
import { GlobeRouteModule } from './globe.route';

// components
import { ConnectedDevicesComponent } from './components/connected-devices/connected-devices.component';
import { GlobeCarouselComponent } from './components/globe-carousel/globe-carousel.component';

// services
import { GlobeDataService } from './services/globe-data.service';

@NgModule({
  declarations: [
    ConnectedDevicesComponent,
    GlobeCarouselComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    CommonModule,
    GlobeRouteModule
  ],
  providers: [
    HttpModule,
    GlobeDataService
  ],
  exports: [
    GlobeCarouselComponent
  ]
})
export class GlobeModule { }
