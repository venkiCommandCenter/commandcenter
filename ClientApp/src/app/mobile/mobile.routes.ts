import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GlobeComponent } from './components/globe/globe.component';
import { PlatformUtilizationComponent } from './components/platform-utilization/platform-utilization.component';
import { PlatformStatusComponent } from './components/platform-status/platform-status.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

const mobileRoutes: Routes = [
    {
        path: '',
        component: SidenavComponent,
        children: [
            {
                path: '',
                redirectTo: 'globe',
                pathMatch: 'full'
            },
            {
                path: 'globe',
                component: GlobeComponent
            },
            {
                path: 'platform-utilization',
                component: PlatformUtilizationComponent
            },
            {
                path: 'platform-status',
                component: PlatformStatusComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(mobileRoutes)
    ], exports: [
        RouterModule
    ]
})

export class MobileRouteModule {}
