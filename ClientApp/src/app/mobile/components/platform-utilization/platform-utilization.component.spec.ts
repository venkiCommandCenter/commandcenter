import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PlatformUtilizationComponent } from './platform-utilization.component';
import { SharedModule } from '../../../shared/shared.module';

describe('PlatformUtilizationComponent (mobile)', () => {
  let component: PlatformUtilizationComponent;
  let fixture: ComponentFixture<PlatformUtilizationComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformUtilizationComponent ],
      imports: [
        HttpClientTestingModule,
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformUtilizationComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have the title 'Platform Maturity'`, () => {
    expect(compiled.querySelector('.subHeader').textContent).toContain('Platform Maturity');
  });

  it(`should have defined 'platformUtilization'`, () => {
    expect(component.platformUtilization).toBeDefined();
  });

  it(`should have defined 'sbgWiseProducts'`, () => {
    expect(component.sbgWiseProducts).toBeDefined();
  });

  it(`should have called 'getData()' with no arguments`, () => {
    spyOn(component, 'getData');
    component.getData();
    expect(component.getData).toBeDefined();
    expect(component.getData).toHaveBeenCalled();
    expect(component.getData).toHaveBeenCalledWith();
  });

  it(`should have called 'getSbgWiseProducts()' with no arguments`, () => {
    spyOn(component, 'getSbgWiseProducts');
    component.getSbgWiseProducts();
    expect(component.getSbgWiseProducts).toBeDefined();
    expect(component.getSbgWiseProducts).toHaveBeenCalled();
    expect(component.getSbgWiseProducts).toHaveBeenCalledWith();
  });

  it(`'checkDecimals' function should return correct output`, () => {
    expect(component.checkDecimals).toBeDefined();
    expect(component.checkDecimals({currentUtilization: '5'})).toBeFalsy();
    expect(component.checkDecimals({currentUtilization: '1.5'})).toBeTruthy();
    expect(component.checkDecimals({currentUtilization: null})).toBeFalsy();
    expect(component.checkDecimals(null)).toBeFalsy();
  });

  it(`'isNull' function should return correct output`, () => {
    expect(component.isNull).toBeDefined();
    expect(component.isNull(null)).toBeTruthy();
    expect(component.isNull('null')).toBeTruthy();
    expect(component.isNull('Mile')).toBeFalsy();
  });
});
