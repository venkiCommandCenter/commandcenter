
import {takeWhile} from 'rxjs/operators';
import { Component, OnInit, DoCheck, IterableDiffers } from '@angular/core';
import { PlatformUtilizationService } from '../../../platform/services/platform-utilization.service';
import { Observable } from 'rxjs';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import { Constants } from '../../../shared/constants';

@Component({
  selector: 'app-platform-utilization',
  templateUrl: './platform-utilization.component.html',
  styleUrls: ['./platform-utilization.component.css'],
  providers: [PlatformUtilizationService]
})
export class PlatformUtilizationComponent implements OnInit {
  public platformUtilization = [];
  public sbgWiseProducts = [];
  private alive: boolean;
  private interval: number;
  private differ: any;
  private progressFontSize = 22;
  private progressFontTop = -10;

  constructor(
    private platformUtilizationService: PlatformUtilizationService,
    private differs: IterableDiffers
  ) {
    this.alive = true;
    this.interval = Constants.intevalTimer;
    this.differ = differs.find([]).create(null);
  }

  ngOnInit() {
    this.getData();
    this.getSbgWiseProducts();
  }

  getData() {
    TimerObservable.create(0, this.interval).pipe(
      takeWhile(() => this.alive))
      .subscribe(() => {
        this.platformUtilizationService.getPlatformUtilization().then((data: any) => {
          this.platformUtilization = data;
          for (let i = 0; i <= this.platformUtilization.length - 1; i++) {
            this.platformUtilization[i].percentage =
              (((this.platformUtilization[i].currentUtilization) / (this.platformUtilization[i].maxUtilization)) * 100).toFixed(0);
          }
        }, (error: any) => {
          console.error(error);
        });
      });
  }

  getSbgWiseProducts() {
    TimerObservable.create(0, this.interval).pipe(
      takeWhile(() => this.alive))
      .subscribe(() => {
        this.platformUtilizationService.getSbgWiseProducts().then((data: any) => {
          this.sbgWiseProducts = data;
        }, (error: any) => {
          console.error(error);
        });
      });
  }

  checkDecimals(platform) {
    if (platform && platform.currentUtilization && platform.currentUtilization.indexOf('.') !== -1) {
      return true;
    }
    return false;
  }

  isNull(val) {
    if (val) {
      if (val.toLowerCase() === 'null') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

}
