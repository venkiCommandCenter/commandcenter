import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PlatformStatusComponent } from './platform-status.component';
import { SharedModule } from '../../../shared/shared.module';

describe('PlatformStatusComponent (mobile)', () => {
  let component: PlatformStatusComponent;
  let fixture: ComponentFixture<PlatformStatusComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlatformStatusComponent ],
      imports: [
        HttpClientTestingModule,
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformStatusComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have the title 'Platform Status'`, () => {
    expect(compiled.querySelector('.subHeader').textContent).toContain('Platform Status');
  });

  it(`should have 'app-sparkline' tag`, () => {
    expect(compiled.querySelector('app-sparkline')).not.toBeNull();
  });

  it(`should have 4 'H4' tags`, () => {
    expect(compiled.querySelectorAll('H4').length).toEqual(4);
  });

  it(`1st 'H4' tag should contain text 'Alert Volume'`, () => {
    expect(compiled.querySelectorAll('H4')[0].textContent).toEqual('Alert Volume');
  });

  it(`2nd 'H4' tag should contain text 'Sentience Health'`, () => {
    expect(compiled.querySelectorAll('H4')[1].textContent).toEqual('Sentience Health');
  });

  it(`3rd 'H4' tag should contain text 'Cloud Services'`, () => {
    expect(compiled.querySelectorAll('H4')[2].textContent).toEqual('Cloud Services');
  });

  it(`4th 'H4' tag should contain text 'Network Availability'`, () => {
    expect(compiled.querySelectorAll('H4')[3].textContent).toEqual('Network Availability');
  });

  it(`should have defined 'cloudServiceModel'`, () => {
    expect(component.cloudServiceModel).toBeDefined();
  });

  it(`should have defined 'ticketServiceModel'`, () => {
    expect(component.ticketServiceModel).toBeDefined();
  });

  it(`should have defined 'networkServiceModel'`, () => {
    expect(component.networkServiceModel).toBeDefined();
  });

  it(`should have defined 'alertVolumeModel'`, () => {
    expect(component.alertVolumeModel).toBeDefined();
  });

  it(`should have defined 'sentianceHealths'`, () => {
    expect(component.sentianceHealths).toBeDefined();
  });

  it(`should have defined 'chartOptions'`, () => {
    expect(component.chartOptions).toBeDefined();
  });

  it(`should have called 'getData()' with no arguments`, () => {
    spyOn(component, 'getData');
    component.getData();
    expect(component.getData).toBeDefined();
    expect(component.getData).toHaveBeenCalled();
    expect(component.getData).toHaveBeenCalledWith();
  });

  it(`'setProgressStatus' function should return correct output`, () => {
    expect(component.setProgressStatus).toBeDefined();
    expect(component.setProgressStatus(100, true)).toEqual('status-success');
    expect(component.setProgressStatus(50, true)).toEqual('status-danger');
    expect(component.setProgressStatus(0, true)).toEqual('status-disable');
    expect(component.setProgressStatus(100, false)).toEqual('status-bg-success');
    expect(component.setProgressStatus(50, false)).toEqual('status-bg-danger');
    expect(component.setProgressStatus(0, false)).toEqual('status-bg-disable');
  });
});
