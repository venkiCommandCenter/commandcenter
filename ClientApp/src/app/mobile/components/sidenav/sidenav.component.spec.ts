import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SidenavComponent } from './sidenav.component';

describe('SidenavComponent (mobile)', () => {
  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;
  let compiled: any;
  const navs = [
    {
      label: 'Globe',
      route: 'globe'
    }, {
      label: 'Platform Maturity',
      route: 'platform-utilization'
    }, {
      label: 'Platform Status',
      route: 'platform-status'
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidenavComponent ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidenavComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have a 'navbar'`, () => {
    expect(compiled.querySelector('.navbar')).not.toBeNull();
  });

  it(`should have a 'sidenav'`, () => {
    expect(compiled.querySelector('.sidenav')).not.toBeNull();
  });

  it(`should have a 'sidenav-body'`, () => {
    expect(compiled.querySelector('.sidenav-body')).not.toBeNull();
  });

  it(`logo image should be 'Honeywell-logo-2015_RGB_Red-sm.png'`, () => {
    expect(compiled.querySelector('.brand-logo').getAttribute('src')).toContain('Honeywell-logo-2015_RGB_Red-sm.png');
  });

  it(`should have assigned 'navs' with ${JSON.stringify(navs)}`, () => {
    expect(component.navs).toEqual(navs);
  });

  it(`should have called 'openNav' function with Event as argument`, () => {
    spyOn(component, 'openNav');
    const mockEvent = {};
    component.openNav(mockEvent);
    expect(component.openNav).toBeDefined();
    expect(component.openNav).toHaveBeenCalled();
    expect(component.openNav).toHaveBeenCalledWith(mockEvent);
  });

  it(`should have called 'closeNav' function with no argument`, () => {
    spyOn(component, 'closeNav');
    component.closeNav();
    expect(component.closeNav).toBeDefined();
    expect(component.closeNav).toHaveBeenCalled();
    expect(component.closeNav).toHaveBeenCalledWith();
  });
});
