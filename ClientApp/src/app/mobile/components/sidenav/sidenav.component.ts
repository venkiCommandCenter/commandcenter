import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SidenavComponent implements OnInit {

  @ViewChild('sidenav') sidenav: ElementRef;
  navs: Array<any> = [
    {
      label: 'Globe',
      route: 'globe'
    }, {
      label: 'Platform Maturity',
      route: 'platform-utilization'
    }, {
      label: 'Platform Status',
      route: 'platform-status'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

  openNav(e: any) {
    e.stopPropagation();
    this.sidenav.nativeElement.style.width = '250px';
  }

  /* Set the width of the side navigation to 0 */
  closeNav() {
    this.sidenav.nativeElement.style.width = '0';
  }

}
