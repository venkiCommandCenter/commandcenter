
import {takeWhile} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { GlobeDataService } from '../../../globe/services/globe-data.service';
import { Observable } from 'rxjs';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import {trigger, state, style, transition, animate} from '@angular/animations';

import { Constants } from '../../../shared/constants';
import { GlobeDirective } from '../../../shared/directives/globe.directive'; // to access methods of directive

@Component({
  selector: 'app-globe',
  templateUrl: './globe.component.html',
  styleUrls: ['./globe.component.css']
})
export class GlobeComponent implements OnInit, OnDestroy {

  public globeOptions: any;
  public globeData: Array<any>;
  public deviceSummary: any = [];
  public newsFeedData: any = {};
  public colorDef = {
    red: 0xFF0000,
    green: 0x00FF7F,
    blue: 0x00FF7F
  };
  private alive: boolean;
  private browserRefreshTimeout: any;
  public isOnlyGlobe = false;
  public spikeData: any;
  private popTimeout: any;
  public businessUnits = [
    'ALL',
    'HBT',
    'PMT',
    'SPS'
  ];
  public fieldMap = {
    latitude: 'lattitude',
    longitude: 'longitude',
    value: 'deviceCount',
    color: 'intensity'
  };
  public regions = [
    {
      name: 'GLOBAL',
      position: [],
      geoLocation: 0
    }, {
      name: 'NORTH AMERICA',
      position: [3.4, 0.98, 670],
      geoLocation: 1
    }, {
      name: 'APAC',
      position: [0.6, 0.4, 1200],
      geoLocation: 2
    }, {
      name: 'EMEA',
      position: [-0.8, 0.7, 1150],
      geoLocation: 3
    }
  ];
  public selectedBu: string = this.businessUnits[0];
  public selectedRegion: any = this.regions[0];
  @ViewChild(GlobeDirective) globe: GlobeDirective;

  constructor(
    private globeDataService: GlobeDataService,
    private route: ActivatedRoute
  ) {
    this.alive = true;
    this.isOnlyGlobe = this.route.snapshot.data['globe'];
  }

  ngOnInit() {
    this.bindNewsSummary();
    this.getDeviceSummary();
    this.drawGlobe();

    this.browserRefreshTimeout = setTimeout(() => {
      document.location.reload();
    }, Constants.intervalGlobeData);
  }

  drawGlobe() {
    // this.selectedBu = this.selectedBu === this.businessUnits[0] ? null : this.selectedBu;
    this.globeOptions = {
      colorDef: this.colorDef,
      isMobileScreen: true,
      isRevolving: this.selectedRegion.geoLocation === 0,
      fieldMap: this.fieldMap,
      setSpikeSize: this.setSpikeSize
    };
    this.globeDataService.getConnectedDevices(this.selectedBu).then((data: any) => {
      this.globeData = data;
    }, (error: any) => {
      console.error(error);
    });
  }

  // Get the device summary data
  getDeviceSummary() {
    TimerObservable.create(0, Constants.intevalTimer).pipe(
      takeWhile(() => this.alive))
      .subscribe(() => {
        // const bu = this.selectedBu === this.businessUnits[0] ? null : this.selectedBu;
        this.globeDataService.getConnectedDeviceSummary(this.selectedBu, this.selectedRegion.geoLocation).then((data: any) => {
          this.deviceSummary = data;
          this.calculateTrend(data);
        }, (error: any) => {
          console.error(error);
        });
      });
  }

  calculateTrend(data: Array<any>) {
    data.forEach(d => {
      if (d.previous90Days) {
        d.trend = (d.value - d.previous90Days) * 100 / d.previous90Days;
        if (!Number.isInteger(d.trend)) {
          d.trend = d.trend.toFixed(2);
        }
      }
    });
  }

  bindNewsSummary() {
    // TimerObservable.create(0, Constants.intevalTimer)
    //   .takeWhile(() => this.alive)
    //   .subscribe(() => {
    //     this.globeDataService.getNewsFeedData().then((data: any) => {
    //       this.newsFeedData = data;
    //     }, (error: any) => {
    //       console.error(error);
    //     });
    //   });
  }

  ngOnDestroy() {
    this.alive = false;
    clearTimeout(this.browserRefreshTimeout);
  }

  isNull(val) {
    if (val) {
      if (val.toLowerCase() === 'null') {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  checkDecimals(val) {
    if (val && val.indexOf('.') !== -1) {
      return true;
    }
    return false;
  }

  trimUnit(val: string) {
    if (val && val.length > 2) {
      return val.substring(0, 2);
    }
    return val;
  }

  formatKpiLabel(val: string) {
    const delimiterIdx = val.lastIndexOf('/');
    const labelParts = [];
    if (delimiterIdx !== -1) {
      labelParts.push(val.substring(0, delimiterIdx).trim());
      labelParts.push(val.substring(delimiterIdx + 1, val.length).trim());
    } else {
      labelParts.push(val);
    }
    return labelParts;
  }

  // spike size callback
  setSpikeSize(val) {
    let spikeMagnitude = 1;
    if (val < 10000) {
      spikeMagnitude = 0.002;
    } else if (val < 100000) {
      spikeMagnitude = 0.0002;
    } else {
      spikeMagnitude = 0.00002;
    }
    return spikeMagnitude * val;
  }

  onBuChange(bu: string) {
     // this.selectedBu = bu = ( bu === this.businessUnits[0] ? null : bu );
     this.selectedBu = bu;
     this.drawGlobe();
     this.globeDataService.getConnectedDeviceSummary(bu, this.selectedRegion.geoLocation).then((data: any) => {
       this.deviceSummary = data;
       this.calculateTrend(data);
     }, (error: any) => {
       console.error(error);
     });
  }

  onRegionChange(reg: any) {
    this.selectedRegion = reg;
    // const bu = ( this.selectedBu === this.businessUnits[0] ? null : this.selectedBu );
    if (reg.geoLocation === 0) {
      this.globe.setRotation(true);
    } else {
      this.globe.setRotation(false);
    }
    this.globe.setTarget(reg.position);
    this.globeDataService.getConnectedDeviceSummary(this.selectedBu, reg.geoLocation).then((data: any) => {
      this.deviceSummary = data;
      this.calculateTrend(data);
    }, (error: any) => {
      console.error(error);
    });
  }

  zoomOut() {
    this.globe.zoom(100);
  }

  zoomIn() {
    this.globe.zoom(-100);
  }

}
