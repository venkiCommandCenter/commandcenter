import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../../environments/environment';

// routes
import { MobileRouteModule } from './mobile.routes';

// components
import { GlobeComponent } from './components/globe/globe.component';
import { PlatformUtilizationComponent } from './components/platform-utilization/platform-utilization.component';
import { PlatformStatusComponent } from './components/platform-status/platform-status.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MobileRouteModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  declarations: [
    GlobeComponent,
    PlatformUtilizationComponent,
    PlatformStatusComponent,
    SidenavComponent
  ]
})
export class MobileModule { }
