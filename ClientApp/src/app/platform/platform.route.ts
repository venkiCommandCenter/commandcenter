import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PlatformUtilizationComponent } from './components/platform-utilization/platform-utilization.component';
import { PlatformStatusComponent } from './components/platform-status/platform-status.component';
import { PlatformCarouselComponent } from './components/platform-carousel/platform-carousel.component';

const platformRoutes: Routes = [
    {
        path: '',
        component: PlatformCarouselComponent
    },
    {
        path: 'platform-utilization',
        component: PlatformUtilizationComponent
    },
    {
        path: 'platform-status',
        component: PlatformStatusComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(platformRoutes)
    ], exports: [
        RouterModule
    ]
})

export class PlatformRouteModule {}
