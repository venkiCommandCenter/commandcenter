import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { PlatformStatusService } from './platform-status.service';
import { LoggerService } from '../../shared/services/logger.service';
import { Constants } from '../../shared/constants';

describe('PlatformStatusService', () => {
  let service: PlatformStatusService;
  let http: HttpClient;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PlatformStatusService,
        LoggerService
      ]
    });
    service = TestBed.get(PlatformStatusService);
    http = TestBed.get(HttpClient);
    httpController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`'getPlatformStatus: GET' should return correct response`, () => {
    const testUrl = Constants.baseURL + 'api/PlatformServices';
    const dummyResponse = {
      'cloudServices': [
        {
          'id': 82,
          'cloudServiceName': 'Azure ',
          'cloudServicePercentage': '99.8',
          'currentStatus': 'Normal'
        }
      ],
      'networkAvailabilities': [
        {
          'id': 0,
          'region': 'North America',
          'currentStatus': 'Normal'
        }
      ]
    };

    http.get(testUrl).subscribe(res => {
      expect(res).toEqual(dummyResponse);
    });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });
});
