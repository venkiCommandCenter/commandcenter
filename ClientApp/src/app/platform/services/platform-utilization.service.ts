import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constants } from '../../shared/constants';
import { LoggerService } from '../../shared/services/logger.service';

@Injectable()
export class PlatformUtilizationService {
  constructor(
    private http: HttpClient,
    private loggerService: LoggerService
  ) { }

  getPlatformUtilization(): Promise<any> {
    const url = Constants.baseURL + 'api/PlatformUtilization';
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        this.loggerService.info(`${url} is called successfully`);
        resolve(res);
      }, err => {
        this.loggerService.error(`${url} failed!`, err);
        reject(err);
      });
    });
  }

  getSbgWiseProducts(): Promise<any> {
    const url = Constants.baseURL + 'api/LiveProducts/118';
    return new Promise((resolve, reject) => {
      this.http.get(url).subscribe(res => {
        this.loggerService.info(`${url} is called successfully`);
        resolve(res);
      }, err => {
        this.loggerService.error(`${url} failed!`, err);
        reject(err);
      });
    });
  }
}
