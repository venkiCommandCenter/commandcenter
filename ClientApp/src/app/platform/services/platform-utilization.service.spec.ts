import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { PlatformUtilizationService } from './platform-utilization.service';
import { LoggerService } from '../../shared/services/logger.service';
import { Constants } from '../../shared/constants';

describe('PlatformUtilizationService', () => {
  let service: PlatformUtilizationService;
  let http: HttpClient;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PlatformUtilizationService,
        LoggerService
      ]
    });
    service = TestBed.get(PlatformUtilizationService);
    http = TestBed.get(HttpClient);
    httpController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`'getPlatformUtilization: GET' should return correct response`, () => {
    const testUrl = Constants.baseURL + 'api/PlatformUtilization';
    const dummyResponse = [
      {
        'id': 118,
        'platformName': 'Products Live',
        'maxUtilization': '4',
        'minUtilization': '0',
        'currentUtilization': '950',
        'currentState': 'Normal',
        'platformUnit': 'Null',
        'planStatus': 'in Dev',
        'sbgName': null,
        'sbgValue': null
      },
      {
        'id': 120,
        'platformName': 'Analytic Models',
        'maxUtilization': '168',
        'minUtilization': '0',
        'currentUtilization': '39',
        'currentState': 'Normal',
        'platformUnit': 'Null',
        'planStatus': null,
        'sbgName': null,
        'sbgValue': null
      }
    ];

    http.get(testUrl).subscribe(res => {
      expect(res).toEqual(dummyResponse);
    });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });

  it(`'getSbgWiseProducts: GET' should return correct response`, () => {
    const testUrl = Constants.baseURL + 'api/LiveProducts/118';
    const dummyResponse = [
      {
        'id': 118,
        'platformName': null,
        'maxUtilization': null,
        'minUtilization': null,
        'currentUtilization': null,
        'currentState': null,
        'platformUnit': null,
        'planStatus': null,
        'sbgName': 'HBT',
        'sbgValue': '100'
      },
      {
        'id': 118,
        'platformName': null,
        'maxUtilization': null,
        'minUtilization': null,
        'currentUtilization': null,
        'currentState': null,
        'platformUnit': null,
        'planStatus': null,
        'sbgName': 'SPS',
        'sbgValue': '500'
      },
    ];

    http.get(testUrl).subscribe(res => {
      expect(res).toEqual(dummyResponse);
    });

    const req = httpController.expectOne(testUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(dummyResponse);
    httpController.verify();
  });
});
