import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

// routes
import { PlatformRouteModule } from './platform.route';

// components
import { PlatformUtilizationComponent } from './components/platform-utilization/platform-utilization.component';
import { PlatformStatusComponent } from './components/platform-status/platform-status.component';
import { PlatformCarouselComponent } from './components/platform-carousel/platform-carousel.component';

@NgModule({
  declarations: [
    PlatformUtilizationComponent,
    PlatformStatusComponent,
    PlatformCarouselComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    PlatformRouteModule
  ],
  providers: [HttpModule],
  exports: [
    PlatformCarouselComponent
  ]
})
export class PlatformModule { }
