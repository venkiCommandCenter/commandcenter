import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-platform-carousel',
  templateUrl: './platform-carousel.component.html',
  styleUrls: ['./platform-carousel.component.css']
})
export class PlatformCarouselComponent implements OnInit {

  activeSlide = 0;

  constructor() { }

  ngOnInit() {
  }

  enableSlide(idx) {
    this.activeSlide = idx;
  }

}
