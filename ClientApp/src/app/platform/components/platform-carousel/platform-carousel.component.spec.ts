import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PlatformCarouselComponent } from './platform-carousel.component';
import { PlatformUtilizationComponent } from '../platform-utilization/platform-utilization.component';
import { PlatformStatusComponent } from '../platform-status/platform-status.component';
import { SharedModule } from '../../../shared/shared.module';

describe('PlatformCarouselComponent', () => {
  let component: PlatformCarouselComponent;
  let fixture: ComponentFixture<PlatformCarouselComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PlatformCarouselComponent,
        PlatformUtilizationComponent,
        PlatformStatusComponent
      ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformCarouselComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it(`should have assigned 'activeSlide' with '0'`, () => {
    expect(component.activeSlide).toEqual(0);
  });

  it(`initially should have the tag 'app-platform-utilization'`, () => {
    expect(compiled.querySelector('app-platform-utilization')).not.toBeNull();
  });

  it(`initially should not have the tag 'app-platform-status'`, () => {
    expect(compiled.querySelector('app-platform-status')).toBeNull();
  });

  it('should have 2 carousel controls', () => {
    expect(compiled.querySelector('.slide-selectors').children.length).toEqual(2);
  });
});
