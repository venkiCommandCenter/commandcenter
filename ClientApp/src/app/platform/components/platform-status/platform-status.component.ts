
import {takeWhile} from 'rxjs/operators';
import { Component, OnInit, QueryList, ElementRef } from '@angular/core';
import { PlatformStatusService } from '../../services/platform-status.service';
import { Observable } from 'rxjs';
import { TimerObservable } from 'rxjs/observable/TimerObservable';

import { Constants } from '../../../shared/constants';

@Component({
    selector: 'app-platform-status',
    templateUrl: './platform-status.component.html',
    styleUrls: ['./platform-status.component.css'],
    providers: [PlatformStatusService]
})

export class PlatformStatusComponent implements OnInit {
    public cloudServiceModel = [];
    public ticketServiceModel = [];
    public networkServiceModel = [];
    public alertVolumeModel = [];
    public sentianceHealths = [];
    public chartOptions = {};
    private alive: boolean;
    private interval: number;
    private isFullscreenView = false;

    constructor(private platformStatusService: PlatformStatusService, private element: ElementRef) {
        this.alive = true;
        this.interval = Constants.intevalTimer;
    }

    ngOnInit() {
        this.getData();
        if (this.element.nativeElement.parentNode.offsetWidth > 1024) {
            this.isFullscreenView = true;
        }
    }

    getData() {
        TimerObservable.create(0, this.interval).pipe(
            takeWhile(() => this.alive))
            .subscribe(() => {
                this.platformStatusService.getPlatformStatus().then((data: any) => {
                    this.cloudServiceModel = data.cloudServices;
                    this.ticketServiceModel = data.ticketSummaries;
                    this.networkServiceModel = data.networkAvailabilities;
                    this.alertVolumeModel = data.alertVolumes;
                    this.sentianceHealths = data.sentianceHealths;
                }, (error: any) => {
                    console.error(error);
                });
            });
    }

    setProgressStatus(val: number, isFont: boolean) {
        if (isFont) {
            if (val >= 99) {
                return 'status-success';
            } else if (val == 0) {
                return 'status-disable';
            } else {
                return 'status-danger';
            }
        } else {
            if (val >= 99) {
                return 'status-bg-success';
            } else if (val == 0) {
                return 'status-bg-disable';
            } else {
                return 'status-bg-danger';
            }
        }
    }
}
