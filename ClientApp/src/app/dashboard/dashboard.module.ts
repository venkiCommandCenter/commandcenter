import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRouteModule } from './dashboard.route';
import { GlobeModule } from '../globe/globe.module';
import { PlatformModule } from '../platform/platform.module';

// components
import { SplitScreensComponent } from './components/split-screens/split-screens.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRouteModule,
    GlobeModule,
    PlatformModule
  ],
  declarations: [SplitScreensComponent]
})
export class DashboardModule { }
