import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SplitScreensComponent } from './split-screens.component';
import { GlobeModule } from '../../../globe/globe.module';
import { PlatformModule } from '../../../platform/platform.module';
import { SharedModule } from '../../../shared/shared.module';

describe('SplitScreensComponent', () => {
  let component: SplitScreensComponent;
  let fixture: ComponentFixture<SplitScreensComponent>;
  let compiled: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SplitScreensComponent
      ],
      imports: [
        RouterTestingModule,
        GlobeModule,
        PlatformModule,
        SharedModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitScreensComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a app-globe-carousel tag', () => {
    expect(compiled.querySelector('app-globe-carousel')).not.toBeNull();
  });

  it('should have a app-platform-carousel tag', () => {
    expect(compiled.querySelector('app-platform-carousel')).not.toBeNull();
  });
});
