import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SplitScreensComponent } from './components/split-screens/split-screens.component';

const dashboardRoutes: Routes = [
    {
        path: '',
        component: SplitScreensComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(dashboardRoutes)
    ], exports: [
        RouterModule
    ]
})

export class DashboardRouteModule {}
